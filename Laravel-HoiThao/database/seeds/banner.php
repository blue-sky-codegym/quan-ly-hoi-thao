<?php

use Illuminate\Database\Seeder;

class banner extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('banner')->insert
    	([
    		['image'=>"http://langmaster.edu.vn/public/files/editor-upload/images/banner-TH.png",'content'=>"",'URL'=>"http://langmaster.edu.vn"],
    		['image'=>"https://repu.vn/wp-content/uploads/2016/08/banner-hoi-thao.jpg",'content'=>"",'URL'=>"http://repu.vn"],
    		['image'=>"http://it.dntu.edu.vn/wp-content/uploads/2014/10/Banner-CNTT-DN_small.jpg",'content'=>"",'URL'=>"http://it.dntu.edu.vn"],
    		['image'=>"https://www.set-edu.com/web/images/NhiTrinh_2016/Banner_DVD_crop.jpg",'content'=>"",'URL'=>"htp://www.set-edu.com"],
            ['image'=>"http://lambanner.com/wp-content/uploads/2016/12/lambanner-thiet-ke-banner-su-kien-hoi-thao-1130x570.jpg",'content'=>"",'URL'=>"htp://google.com"],
    	]);
    }
}
