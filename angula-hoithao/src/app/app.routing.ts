import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { NavbarAndBannerComponent } from './homeComponent/navbar-and-banner/navbar-and-banner.component';
import { LoginComponent } from './authComponent/login/login.component';
import { RegistrationComponent } from './authComponent/registration/registration.component';
import { NextSeminarComponent } from './homeComponent/next-seminar/next-seminar.component';
import { DetailSeminarComponent } from './showSeminar/detail-seminar/detail-seminar.component';
import { FilterSeminarComponent } from './homeComponent/filter-seminar/filter-seminar.component';
import { HoithaosapdienraComponent }from './seminarnext/hoithaosapdienra/hoithaosapdienra.component';
import { BeforeLoginService } from './Sevice/activateRouter/BeforeLogin/before-login.service';
import { AfterLoginService } from './Sevice/activateRouter/AfterLogin/after-login.service';
import { AccessAdminService } from './Sevice/activateRouter/admin/access-admin.service';


const routes: Routes = [
  {
    path: '', component: NavbarAndBannerComponent,
    children: [
      { path: '', component: NextSeminarComponent },
      { path: 'seminar/detail/:id', component: DetailSeminarComponent },
      { path: 'filter-seminar', component: FilterSeminarComponent },
      // { path: 'hoithaosapdienra', component:HoithaosapdienraComponent}
    ]
  },
  { path: 'login', component: LoginComponent, canActivate: [BeforeLoginService] },
  { path: 'register', component: RegistrationComponent, canActivate: [BeforeLoginService] },

  {
    path: 'admin',
    redirectTo: 'admin/user',
    pathMatch: 'full',
  }, {
    path: 'admin',
    component: AdminLayoutComponent,
    canActivate: [AfterLoginService, AccessAdminService],
    children: [
      {
        path: '',
        loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
      }]
  },
  {
    path: 'myAccout',
    redirectTo: 'myAccout/user',
    pathMatch: 'full',
  }, {
    path: 'myAccout',
    component: AdminLayoutComponent,
    canActivate: [AfterLoginService],
    children: [
      {
        path: '',
        loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
      }]
  },
  {
    path: '**',
    redirectTo: ''
  }
];


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
