import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  apiUrl = "http://127.0.0.1:8000/api";
  constructor(private http: HttpClient) { }


  updateUser(form,id:number){
    return this.http.post(`${this.apiUrl}/editInformationUser/${id}`,form)
  }

  updateUserAvatar(formData,id:number){
    return this.http.post(`${this.apiUrl}/editInformationUser/${id}/image`,formData)
  }


  commentUser(formData){
   
    return this.http.post(`${this.apiUrl}/postComment`,formData)
  }
}
