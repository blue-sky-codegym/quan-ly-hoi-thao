<?php

use Illuminate\Database\Seeder;

class imageSeminar extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('imageSeminar')->insert
    	([
    		['image'=>"http://vietnam-tourism.com/imguploads/news/2017/T5/Hoithao-Golf-DN-1.jpg",'seminar_id'=>1],
    		['image'=>"http://vietnamtourism.gov.vn/images/2018/Thang3/HoithaogiaiphapXTDL1.jpg",'seminar_id'=>2],
    		['image'=>"http://hoiluatgiavn.org.vn:8080/files/HTQTBD8.jpg",'seminar_id'=>3],
    		['image'=>"http://baokhanhhoa.vn/dataimages/201611/original/images1185182_hoithao.jpg",'seminar_id'=>4],
    		['image'=>"http://image.vovworld.vn/w730/uploaded/vovworld/ovunhuo/2018_08_28/hoithao_knto.jpg",'seminar_id'=>5],
    		['image'=>"http://ueb.edu.vn/Uploads/image/News/Thumbnails/2010/6/Thumbnails28072010020735.jpg",'seminar_id'=>6],
    	]);
    }
}
