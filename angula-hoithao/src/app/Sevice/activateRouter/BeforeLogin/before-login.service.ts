import { Injectable } from '@angular/core';
import { TokenService } from 'app/authComponent/authSevice/token.service';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class BeforeLoginService implements CanActivate {
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    
    return !this.Token.loggedIn();
  }
  constructor(private Token: TokenService) { }

}
