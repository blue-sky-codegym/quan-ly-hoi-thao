import { TestBed } from '@angular/core/testing';

import { InviteSeminarService } from './invite-seminar.service';

describe('InviteSeminarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InviteSeminarService = TestBed.get(InviteSeminarService);
    expect(service).toBeTruthy();
  });
});
