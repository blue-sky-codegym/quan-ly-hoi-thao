<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Seminar;


class userNotification extends Model
{
    protected $table='NotificationUser';
    protected $fillable = ['content'];
    public function seminars(){
        return $this->belongsTo(Seminar::class);
    }
    public function user(){
        return $this->belongsToMany(User::class);
    }
}
