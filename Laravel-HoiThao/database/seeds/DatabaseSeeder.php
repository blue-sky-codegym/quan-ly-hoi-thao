<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(auditlog::class);
        
        $this->call(user::class); 
        $this->call(category::class);
        $this->call(seminar::class);
        $this->call(contact::class);
        $this->call(guestParticipantRegisterList::class);
        $this->call(imageSeminar::class);

        $this->call(banner::class);
        $this->call(ads::class);
        $this->call(UserNotifica::class);
        //  $this->call(commnet::class);
    }
}
