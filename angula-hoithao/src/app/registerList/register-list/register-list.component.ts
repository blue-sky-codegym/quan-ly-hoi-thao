import { Component, OnInit } from '@angular/core';
import { RegisterService } from 'app/Sevice/RegisterSeminar/register.service';
import { registerSeminar } from 'app/Model/registerSeminar.module';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-register-list',
  templateUrl: './register-list.component.html',
  styleUrls: ['./register-list.component.scss']
})
export class RegisterListComponent implements OnInit {

  seminar_id: number;
  ListGuest = [];
  ListUser =[];
  constructor(private registerService: RegisterService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.seminar_id = +this.route.snapshot.paramMap.get('id');

    this.registerService.getListGuest(this.seminar_id).subscribe(data => this.ListGuest = data);
    
    this.registerService.getListUser(this.seminar_id).subscribe(next =>this.ListUser = next);
 
  }

}
