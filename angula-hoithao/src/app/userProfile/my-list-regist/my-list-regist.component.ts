import { Component, OnInit } from '@angular/core';
import { RegisterService } from 'app/Sevice/RegisterSeminar/register.service';
import { Seminar } from 'app/Model/Seminar.module';
import { User } from 'app/user/user';

@Component({
  selector: 'app-my-list-regist',
  templateUrl: './my-list-regist.component.html',
  styleUrls: ['./my-list-regist.component.scss']
})
export class MyListRegistComponent implements OnInit {
  user: any;
  RegisterSeminar: Object;
  RegisterSponsor: Object;
  constructor(private registerService: RegisterService) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.registerService.getUserRegisterSeminar(this.user.id).subscribe(data => this.RegisterSeminar = data)
    this.registerService.getSponsorUserRegister(this.user.id).subscribe(data => this.RegisterSponsor = data)
  }
}


