<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Connect extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("Seminars", function (Blueprint $table) { 
                $table->foreign('categorys_id')
                ->references('id')
                ->on('categorys')
                ->onDelete('cascade');
        });

        Schema::table('registerRole', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });

        Schema::table('user_seminars_sponsorRegisterList', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('seminar_id')
                ->references('id')->on('Seminars')
                ->onDelete('cascade');
        });


        Schema::table('user_seminars_comment', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('seminar_id')
                ->references('id')->on('Seminars')
                ->onDelete('cascade');
        });

        // Schema::table('imageSeminar', function (Blueprint $table) {
        //     $table->foreign('seminar_id')
        //         ->references('id')->on('Seminars')
        //         ->onDelete('cascade');
        // });

        Schema::table('user_serminars_invite', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('seminar_id')
                ->references('id')->on('Seminars')
                ->onDelete('cascade');
        });
        Schema::table('user_seminar_interestList', function (Blueprint $table) {
            $table->foreign('seminar_id')
                ->references('id')->on('Seminars')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });

        Schema::table('user_seminars_participantRegisterList', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('seminar_id')
                ->references('id')->on('Seminars')
                ->onDelete('cascade');
        });
        
        Schema::table("guestParticipantRegisterList", function (Blueprint $table) { 
            $table->foreign('seminar_id')
            ->references('id')
            ->on('Seminars')
            ->onDelete('cascade');
         });

        Schema::table("NotificationUser", function (Blueprint $table) { 
            $table->foreign('seminar_id')
            ->references('id')
            ->on('Seminars')
            ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
