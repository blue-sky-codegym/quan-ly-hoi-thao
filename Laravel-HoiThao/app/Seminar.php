<?php

namespace App;

use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

use Illuminate\Database\Eloquent\Model;

class Seminar extends Model implements AuditableContract
{
    use \OwenIt\Auditing\Auditable;

    protected $table='Seminars';
    protected $fillable = ['seminarName', 'content','host','adress','heldTime','registrationTime','status','summary','capacity','categorys_id'];
    public function categorys(){
        return $this->belongsTo(Categorys::class);
    }

    public function userComment()
    {
        return $this->belongsToMany(User::class, 'user_seminars_comment')->withPivot('comment','userName');
    }

    public function userInvite()
    {
        return $this->belongsToMany(User::class, 'user_serminars_invite')->withPivot('content');
    }

    public function userInterstList()
    {
        return $this->belongsToMany(User::class, 'user_seminar_interestList')->withPivot('note');
    }

    public function userSponsorRegisterList()
    {
        return $this->belongsToMany(User::class, 'user_seminars_sponsorRegisterList')->withPivot('content','closeTheList');
    }

    public function userParticipantRegisterList()
    {
        return $this->belongsToMany(User::class, 'user_seminars_participantRegisterList')->withPivot('closeTheList');
    }

    public function guestParticipantRegisterList()
    {
        return $this->belongsTo(guestParticipantRegisterList::class,'seminar_id');
    }

    protected $auditInclude = [
        'seminarName',
    ];
}
