export class ads {
    id: number;
    name: string;
    content: string;
    image: string;
    URL: string;
    status: boolean;
}