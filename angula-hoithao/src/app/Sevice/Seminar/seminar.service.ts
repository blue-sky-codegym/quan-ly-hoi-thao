import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Seminar } from 'app/Model/Seminar.module';

@Injectable({
  providedIn: 'root'
})

export class SeminarService {

  apiUrl = "http://127.0.0.1:8000/api/seminars";

  constructor(private http: HttpClient) { }

  getSeminar(): Observable<Seminar[]> {
    return this.http.get<Seminar[]>(`${this.apiUrl}/list`);
  }
  getSeminarnext(): Observable<Seminar[]> {
    return this.http.get<Seminar[]>(`${this.apiUrl}/listseminarnext`);
  }
  getSeminardone(): Observable<Seminar[]> {
    return this.http.get<Seminar[]>(`${this.apiUrl}/listdone`);
  }
  addSeminar(seminar: Seminar): Observable<Seminar> {
    return this.http.post<Seminar>(`${this.apiUrl}/store`, seminar)
  }
  deleteSeminar(id): Observable<any> {
    return this.http.delete(`${this.apiUrl}/delete/${id}`)
  }
  getSeminarByid(id: number): Observable<Seminar> {
    return this.http.get<Seminar>(`${this.apiUrl}/getSeminar/${id}`)
  }
  updateSeminar(newSeminar, id: number) {
    return this.http.put(`${this.apiUrl}/update/${id}`, newSeminar)
  }
  getComment(id: number){
    return this.http.get(`${this.apiUrl}/comment/${id}`)
  }

  updateComment(newComment,id: number){
    return this.http.put(`${this.apiUrl}/comment/edit/${id}`, newComment)
  }

  deleteComment(id) {
    return this.http.delete(`${this.apiUrl}/deleteComment/${id}`)
  }

}