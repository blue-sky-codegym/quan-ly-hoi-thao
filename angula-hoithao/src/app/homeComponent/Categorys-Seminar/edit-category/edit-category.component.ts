import { Component, OnInit } from '@angular/core';
import { Category } from 'app/Model/category';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CategoryService } from 'app/Sevice/category.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})
export class EditCategoryComponent implements OnInit {
  categorys: Category;
  editFormGroup: FormGroup;

  constructor
    (
      private categoryService: CategoryService,
      private editFormBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router
    ) { }

  ngOnInit() {
    this.editFormGroup = this.editFormBuilder.group(
      {
        categoryName: ['', [Validators.required, Validators.minLength(5)]],
        description: ['', [Validators.required, Validators.minLength(10)]],
      }
    )

    const id = +this.route.snapshot.paramMap.get('id');
    this.categoryService.getCategoryById(id).subscribe(
      next => {
        this.categorys = next;
        this.editFormGroup.patchValue(this.categorys)
      },
      error => {
        console.log(error);
      }
    )
  }

  onSubmit() {
    this.categoryService.updateCategory(this.editFormGroup.value, this.categorys.id).subscribe(
      next => (
        alert("Bạn đã cập nhập thành công"),
        this.router.navigate(['/admin/table'])),
      error => console.log(error)
    )
  }

}
