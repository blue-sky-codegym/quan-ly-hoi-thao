import { TestBed } from '@angular/core/testing';

import { EditParticipateListService } from './edit-participate-list.service';

describe('EditParticipateListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditParticipateListService = TestBed.get(EditParticipateListService);
    expect(service).toBeTruthy();
  });
});
