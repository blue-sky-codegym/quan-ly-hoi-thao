<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\userNotification;
class NotificationController extends Controller
{
    public function index(Request $request){
        $today=date("Y/m/d");
        $dateend=date('Y/m/d', strtotime(' +2 day'));
        // $expiry = (new DateTime($toBeComparedDate))->format('Y-m-d');
        $notification = userNotification::where('user_id',$request[0])->where('trangthai',false)->where('ngaydienrahoithao','>',$today)->where('ngaydienrahoithao','<',$dateend)->get();
        // $notification= notification_seminar::find($request[0]);
        return $notification;
    }
    public function updateStatus(Request $request){
        // $arr=$request[0];
        // for($x = 0; $x <$request[1]; $x++) {
        //     $statusnotification=notification_seminar::find($arr[$x]);
        //     $statusnotification->trangthai =true;
        //     $statusnotification->save();
        // }
        $today=date("Y/m/d");
        $dateend=date('Y/m/d', strtotime(' +2 day'));
        $notification = userNotification::where('user_id',$request[0])->where('trangthai',false)->where('ngaydienrahoithao','>',$today)->where('ngaydienrahoithao','<',$dateend)->get();
        foreach( $notification as $item){
            $statusnotification=userNotification::find($item->id);
            $statusnotification->trangthai=true;
            $statusnotification->save();
        }
        
    }
}




