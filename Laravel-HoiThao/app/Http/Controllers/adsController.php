<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ads;
use SebastianBergmann\Environment\Console;

class adsController extends Controller
{
    public function index()
    {
        $ad = Ads::all();
        return $ad;
    }

    public function show($id)
    {
        $ad = Ads::findOrFail($id);
        return $ad;
        
    }

    public function store(Request $request)
    {
        $ads = new Ads();
        $ads->name = $request->input('name');
        $ads->content = $request->input('content');
        $ads->URL = $request->input('URL');
        
        $image = $request->input('image');
        $image = str_replace('data:image/jpeg;base64,', '', $image);
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10).'.'.'png';
        $ads->image = asset('storage/image/'.$imageName);
        \File::put(storage_path(). '/app/public/image/' . $imageName, base64_decode($image));
        $ads->save();
        return $ads;
        
    }

    public function update(Request $request,$id)
    {
        $ad = Ads::findOrFail($id);
        $ad->update($request->all());
        return response()->json($ad,200);
    }

    public function delete($id)
    {
        $ad = Ads::find($id);
        $ad->delete();
    }
}
