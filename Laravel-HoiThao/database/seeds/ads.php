<?php

use Illuminate\Database\Seeder;

class ads extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('Ads')->insert
    	([
    		['name'=>'TiKi','content'=>'Mua Online đồ Điện tử, Điện Lạnh, đồ Công Nghệ, gia dụng, thực phẩm giá tốt mỗi ngày.','image'=>'http://cafefcdn.com/thumb_w/650/2017/tiki-vng-1486623105251.png','URL'=>'https://tiki.vn','status'=>false],
    		['name'=>'CodeGym','content'=>'TRỞ THÀNH LẬP TRÌNH VIÊN FULL-STACK TRONG 16 TUẦN Học được – Làm được – Có việc ngay','image'=>'https://lh3.googleusercontent.com/dI7skLtvE0UXitQsAAzaRlAKhn0fyKRzJSEDIqQl_bfAE7ReXvBGpRa2ITvs-DhyQyzeKd8E=w1080-h608-p-no-v0','URL'=>'https://codegym.vn/khoa-hoc-lap-trinh-danh-cho-nguoi-moi-bat-dau/','status'=>false],
            ['name'=>
            'Shopee','content'=>'Fado Đơn Vị Duy Nhất Tại Việt Nam Mua Hàng Amazon Có Hóa Đơn Chứng Từ Nguồn Gốc. Hỗ Trợ Bảo Hành, Bảo Hiểm Hàng Hóa, Mua Săm Tiện Lợi Và Nhận Hàng Nhanh Chóng. Mua Ngay! Giao hàng miễn phítận nhà. Hỗ trợ rủi ro hàng hóa. Miễn thủ tục hải quan. Thanh toán linh hoạt.',
            'image'=>'https://cdngarenanow-a.akamaihd.net/shopee/shopee-pcmall-live-sg/assets/ca5d12864c12916c05640b36e47ac5c9.png','URL'=>'https://www.shopee.vn/','status'=>false],
    	]);
    }
}
