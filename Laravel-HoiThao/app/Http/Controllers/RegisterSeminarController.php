<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GuestParticipantRegisterList;
use App\ParticipantRegisterList;
use App\User;
use App\Seminar;
use App\InterestList;
use App\SponsorRegisterList;
use App\userNotification;

class RegisterSeminarController extends Controller
{
  public function store(Request $request,$id)
    {
        $registerSeminar = new GuestParticipantRegisterList;
        $registerSeminar->name = $request->input('name');
        $registerSeminar->adress = $request->input('adress');
        $registerSeminar->email = $request->input('email');
        $registerSeminar->phone = $request->input('phone');
        $registerSeminar->dob = $request->input('dob');
        $registerSeminar->seminar_id = $id;
        $registerSeminar->save();
        return response()->json($registerSeminar, 201);
    }
    
  public function storeUser(Request $request){
        $check = true;
        $user = User::find($request[0]);
        $seminar= Seminar::find($request[1]);
        $userReisters = ParticipantRegisterList::all();
        foreach($userReisters as $item){
            if($item->user_id == $user->id && $item->seminar_id == $seminar->id){
              $check = false;
            }
        }
        if($check === true){
          $notification_seminar = new userNotification;
          $notification_seminar->ngaydienrahoithao=$seminar->heldTime;
          $notification_seminar->seminar_id=$seminar->id;
          $notification_seminar->user_id=$user->id;
          $notification_seminar->content="Hội thảo còn 1 ngày nửa diển ra";
          $notification_seminar->tenhoithao=$seminar->seminarName;
          $notification_seminar->save();
          $user->participantRegisterList()->attach($seminar);
        } 
        return response()->json($check, 201);
    }

  public function userInterest(Request $request){
        $check = true;
        $user = User::find($request[0]);
        $seminar= Seminar::find($request[1]);
        $InterestList = InterestList::all();
        foreach($InterestList as $item){
            if($item->user_id == $user->id && $item->seminar_id == $seminar->id){
            $check = false;
           $user->interstList()->detach($seminar);
            }
        }
        if($check === true){
           $user->interstList()->attach($seminar);
        }
        return response()->json($check, 201);
      }

  public function checkInterest(Request $request){
        $check = false;
        $user = User::find($request[0]);
        $seminar= Seminar::find($request[1]);
        $InterestList = InterestList::all();
        foreach($InterestList as $item){
            if($item->user_id == $user->id && $item->seminar_id == $seminar->id){
            $check = true;
            }
        }
        return response()->json($check, 201);
      }

  public function getListGuest($id){
      $listGuest = GuestParticipantRegisterList::where('seminar_id',$id)->get();
       return $listGuest;
  }

  public function getListUser($id)
  {
      $listUser_ID = ParticipantRegisterList::where('seminar_id',$id)->get();
      foreach($listUser_ID as $item) {
      $listUser[] = User::where('id',$item->user_id)->get();      
      }    
      foreach($listUser as $item){
        $list[] =  $item[0];         
      }
    return ($list);
  }

  public function Sponsor(Request $request){    
      $user = User::find($request[0]);        
      $seminar = Seminar::find($request[1]);        
      $content = $request[2];
      $content = $content['content'];
      $user->sponsorRegisterList()->attach($seminar,['content'=>$content]);
      return response()->json($content, 201);
  }

  public function getSponsor($id)
  {
      $listSponsor_id = SponsorRegisterList ::where('seminar_id',$id)->get();
      foreach($listSponsor_id as $item){
          $userList = User:: where('id',$item->user_id)->get();
          foreach ($userList as $user) {
              $user->content = $item->content;
          }            
          $listSponsor[] = $userList; 
      }     
      foreach($listSponsor as $item){
          $list[] = $item[0];
      }
      return $list;
  }
   public function getSeminarUserRegister($id)
   {
      $RegisterList = ParticipantRegisterList::where('user_id',$id)->get();
      foreach($RegisterList as $item){
        $seminarRegister[] = Seminar::where('id',$item->seminar_id)->get();
        foreach($seminarRegister as $seminar){
          $seminarregister[] = $seminar[0];
        }
      }
      foreach($seminarRegister as $list){
        $seminarList[] = $list[0];
      }
      return response()->json($seminarList, 201);
   }

   public function getSponsorUserRegister($id)
   {    $UserSponsor =[];
        $Sponsor = [];
       $SponsorList = SponsorRegisterList::where('user_id',$id)->get();

       foreach ($SponsorList as $item){
           $UserSponsor[] = Seminar::where('id',$item->seminar_id)->get();
           }
       foreach ($UserSponsor as $SponsorList){
           $Sponsor[] = $SponsorList[0];
       }
       return response()->json($Sponsor, 201);
   }

}
