import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClickSeminarComponent } from './click-seminar.component';

describe('ClickSeminarComponent', () => {
  let component: ClickSeminarComponent;
  let fixture: ComponentFixture<ClickSeminarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClickSeminarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClickSeminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
