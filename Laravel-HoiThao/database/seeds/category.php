<?php

use Illuminate\Database\Seeder;

class category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorys')->insert
        ([
            [ 'categoryName' => 'Hội thảo Khoa Học-Công Nghệ','description'=>'Là những hội thảo có nội dung liên quan đến khoa học và công nghệ'],
            [ 'categoryName' => 'Hội Thảo Xã Hội','description'=>'Là những hội thảo có nội dung liên quan đến xã hội'],
            [ 'categoryName' => 'Hội Thảo Kinh Tế','description'=>'Là những hội thảo có nội dung liên quan đến kinh tế'],
            [ 'categoryName' => 'Hội Thảo Đời Sống','description'=>'Là những hội thảoo có nội dung liên quan đến đời sống'],
            [ 'categoryName' => 'Hội Thảo Quốc Tế','description'=>'Là những hội thảo thuộc lĩnh vực quốc tế'],
        ]);
    }
}
