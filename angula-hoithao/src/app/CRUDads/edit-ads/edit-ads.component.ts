import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ads } from 'app/Model/ad.module';
import { AdsService } from 'app/Sevice/ads/ads.service';
import { Location } from '@angular/common';
import { AlertService } from 'ngx-alerts';

@Component({
  selector: 'app-edit-ads',
  templateUrl: './edit-ads.component.html',
  styleUrls: ['./edit-ads.component.scss']
})
export class EditAdsComponent implements OnInit {
  editAdsForm: FormGroup;
  ad: ads;

  constructor(
    private adService: AdsService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    public location: Location,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.editAdsForm = this.fb.group({
      name: ['', [Validators.required]],
      content: ['', [Validators.required]],
      image: ['lol'],
      URL: ['', [Validators.required]],
      status: ['false'],
    });

    const id = +this.route.snapshot.paramMap.get('id');
    this.adService.getAdById(id).subscribe(
      next => {
        this.ad = next;
        this.editAdsForm.patchValue(this.ad);
      },
      error => {
        console.log(error);
        this.ad = null;
      }
    );
  }

  onSubmit() {
    this.adService.updateAd(this.editAdsForm.value, this.ad.id).subscribe()
    this.alertService.info('Bạn đã sửa thành công'),
    this.location.back();

  }

}
