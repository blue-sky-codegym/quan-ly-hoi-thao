import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';
import { NavbarModule } from './shared/navbar/navbar.module';
import { FooterModule } from './shared/footer/footer.module';
import { SidebarModule } from './sidebar/sidebar.module';
import { HttpClientModule } from '@angular/common/http';
import { AdminLayoutModule } from './layouts/admin-layout/admin-layout.module';

import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { DoneSeminarComponent } from './homeComponent/done-seminar/done-seminar.component';
import { NavbarAndBannerComponent } from './homeComponent/navbar-and-banner/navbar-and-banner.component';
import { NextSeminarComponent } from './homeComponent/next-seminar/next-seminar.component';
import { SlideSeminarComponent } from './homeComponent/slide-seminar/slide-seminar.component';
import { ClickSeminarComponent } from './homeComponent/click-seminar/click-seminar.component';
import { AdsComponent } from './homeComponent/ads/ads.component';
import { FooterComponent } from './homeComponent/Footer/footer.component';
import { OldSeminarComponent } from './homeComponent/old-seminar/old-seminar.component';
import { LoginComponent } from './authComponent/login/login.component';
import { RegistrationComponent } from './authComponent/registration/registration.component';
import { DetailSeminarComponent } from './showSeminar/detail-seminar/detail-seminar.component';
import { CommentSeminarComponent } from './showSeminar/comment-seminar/comment-seminar.component';
import { FormRegisterComponent } from './showSeminar/form-register/form-register.component';
import { FilterSeminarComponent } from './homeComponent/filter-seminar/filter-seminar.component';
import { HoithaosapdienraComponent } from './seminarnext/hoithaosapdienra/hoithaosapdienra.component';
import { AlertModule } from 'ngx-alerts';



@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    RouterModule,
    HttpModule,
    NavbarModule,
    HttpClientModule,
    FooterModule,
    SidebarModule,
    AppRoutingModule,
    AdminLayoutModule,
    ReactiveFormsModule,
    AlertModule.forRoot({ maxMessages: 5, timeout: 5000, position: 'right' }),
  ],
  declarations: [
    AppComponent,
    NavbarAndBannerComponent,
    NextSeminarComponent,
    DoneSeminarComponent,
    ClickSeminarComponent,
    LoginComponent,
    RegistrationComponent,
    AdsComponent,
    FooterComponent,
    OldSeminarComponent,
    SlideSeminarComponent,
    AdminLayoutComponent,
    DetailSeminarComponent,
    CommentSeminarComponent,
    FormRegisterComponent,
    FilterSeminarComponent,
    HoithaosapdienraComponent,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
