import { Routes } from '@angular/router';

import { HomeComponent } from '../../home/home.component';
import { UserComponent } from '../../user/user.component';
import { TablesComponent } from '../../tables/tables.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { AddSeminarComponent } from './../../CRUDseminar/add-seminar/add-seminar.component';
import { EditSeminarComponent } from './../../CRUDseminar/edit-seminar/edit-seminar.component';
import { DetailSeminarComponent } from './../../CRUDseminar/detail-seminar/detail-seminar.component';
import { AddAdsComponent } from './../../CRUDads/add-ads/add-ads.component';
import { EditAdsComponent } from './../../CRUDads/edit-ads/edit-ads.component';
import { ListAdsComponent } from './../../CRUDads/list-ads/list-ads.component';
import { AddCategoryComponent } from 'app/CRUDcategory/add-category/add-category.component';
import { EditCategoryComponent } from 'app/CRUDcategory/edit-category/edit-category.component';
import { ListUserComponent } from 'app/CRUDuser/list-user/list-user.component';
import { DetailUserComponent } from 'app/CRUDuser/detail-user/detail-user.component';
import { ListcontactComponent } from 'app/CRUDcontact/listcontact/listcontact.component';
import { EditcontactComponent } from 'app/CRUDcontact/editcontact/editcontact.component';
import { AddcontactComponent } from 'app/CRUDcontact/addcontact/addcontact.component';
import { RegisterListComponent } from './../../registerList/register-list/register-list.component';
import { MyListRegistComponent } from './../../userProfile/my-list-regist/my-list-regist.component';
import { ListSponsorComponent } from './../../registerList/list-sponsor/list-sponsor.component';
import { SendmailComponent} from './../../sendmail/sendmail/sendmail.component'
import { AfterLoginService } from 'app/Sevice/activateRouter/AfterLogin/after-login.service';
import { EditSeminarService } from 'app/Sevice/activateRouter/Editseminar/edit-seminar.service';
import { ListFeedbackComponent } from 'app/FeedBack/list-feedback/list-feedback.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard', component: HomeComponent },
    { path: 'user', component: UserComponent },
    { path: 'table', component: TablesComponent },
    { path: 'typography', component: TypographyComponent },
    { path: 'icons', component: IconsComponent },
    { path: 'maps', component: MapsComponent },
    { path: 'notifications', component: NotificationsComponent },
    { path: 'upgrade', component: UpgradeComponent },
    { path: 'addSeminar', component: AddSeminarComponent },
    { path: 'editSeminar/:id', component: EditSeminarComponent, canActivate: [AfterLoginService, EditSeminarService] },
    { path: 'detailSeminar/:id', component: DetailSeminarComponent },
    { path: 'addAds', component: AddAdsComponent },
    { path: 'editAds/:id', component: EditAdsComponent, canActivate: [AfterLoginService,] },
    { path: 'listAds', component: ListAdsComponent },
    { path: 'addCategory', component: AddCategoryComponent },
    { path: 'editCategory/:id', component: EditCategoryComponent, canActivate: [AfterLoginService,] },
    { path: 'infomationUser', component: ListUserComponent },
    { path: 'detailUser/:id', component: DetailUserComponent },
    { path: 'listContact', component: ListcontactComponent },
    { path: 'editContact/:id', component: EditcontactComponent },
    { path: 'addContact', component: AddcontactComponent },
    { path: 'registerList/:id', component: RegisterListComponent },
    { path: 'listSponsor/:id', component: ListSponsorComponent },
    { path: 'MyList-RegistSeminar', component: MyListRegistComponent },
    { path: 'listFeedBack', component: ListFeedbackComponent }
];
