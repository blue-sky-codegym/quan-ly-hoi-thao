import { Component, OnInit } from '@angular/core';
import { TokenService } from './../authSevice/token.service';
import { Router } from '@angular/router';
import { AuthSeviceService } from './../../authComponent/authSevice/auth-sevice.service';
import { ChangeStatusService } from './../../authComponent/authSevice/change-status.service';
import { User } from 'app/user/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  public form = {
    email: null,
    password: null
  };

  public error = null;
  public user: User

  constructor(
    private AuthSevice: AuthSeviceService,
    private Token: TokenService,
    private router: Router,
    private changeStatus: ChangeStatusService
  ) { }

  onSubmit() {
    this.AuthSevice.login(this.form).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  handleResponse(data) {
    localStorage.setItem('user', JSON.stringify(data.user));
    this.Token.handle(data.access_token);
    this.changeStatus.changeAuthStatus(true);
    this.router.navigateByUrl('home');
  }

  handleError(error) {
    this.error = error.error.error;
  }

  ngOnInit() {
  }

}
