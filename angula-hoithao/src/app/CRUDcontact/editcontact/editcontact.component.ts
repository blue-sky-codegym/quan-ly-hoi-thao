import { Component, OnInit } from '@angular/core';
import { Contact } from 'app/Model/Contact.module';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ContactService } from 'app/Sevice/Contact/contact.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Subscription } from 'rxjs';
import { AlertService } from 'ngx-alerts';

@Component({
  selector: 'app-editcontact',
  templateUrl: './editcontact.component.html',
  styleUrls: ['./editcontact.component.scss']
})
export class EditcontactComponent implements OnInit {

  contact: Contact;
  editForm: FormGroup;

  constructor(private contactSevice: ContactService,
    private fb: FormBuilder, private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService) { }

  ngOnInit() {
    this.editForm = this.fb.group(
      {
        name: ['', [Validators.required, Validators.minLength(4)]],
        adress_1: ['', [Validators.required, Validators.minLength(3)]],
        adress_2: ['', [Validators.required, Validators.minLength(3)]],
        email: ['', [Validators.required, Validators.minLength(3)]],
        phone: ['', [Validators.required, Validators.minLength(3)]],
      }
    )

    const id = +this.route.snapshot.paramMap.get('id');
    this.contactSevice.getbyIdContact(id).subscribe(
      next => {
        this.contact = next;
        this.editForm.patchValue(this.contact);
      },
      error => {
        console.log(error);
        this.contact = null;
      }
    );
  }

  onSubmit() {
    this.contactSevice.updateContact(this.editForm.value, this.contact.id).subscribe(
      next => (
        this.alertService.success('Bạn đã sửa thành công'),
        this.router.navigate(['/admin/listContact']
        ))
    )
  }
}
