<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\Feedback;

class ContactController extends Controller
{         
    public function index()
    {
        $contact = Contact::all();
        return $contact;
    }

    public function show($id)
    { 
        $contact = Contact::find($id);
        return $contact;
        
    }

    public function store(Request $request)
    {    
        $contact = new Contact();     
        $contact->name = $request->input('name');
        $contact->adress_1 = $request->input('adress_1');
        $contact->adress_2 = $request->input('adress_2');
        $contact->phone = $request->input('phone');
        $contact->email = $request->input('email');       
        $contact->save();
        return $contact;
    }

    public function feedback(Request $request){
        $feck = new Feedback();     
        $feck->yourName = $request->input('yourName');
        $feck->email = $request->input('email');
        $feck->subject = $request->input('subject');
        $feck->content = $request->input('content');            
        $feck->save();
        return $feck;
    }

    public function update(Request $request,$id)
    {
        $contact = Contact::findOrFail($id);
        $contact->name = $request->input('name');
        $contact->adress_1 = $request->input('adress_1');
        $contact->adress_2 = $request->input('adress_2');
        $contact->phone = $request->input('phone');
        $contact->email = $request->input('email');       
        $contact->save();
        return $contact;
    }

    public function delete($id)
    {
        $contact = Contact::find($id);
        $contact->delete();
    }    
  
}
