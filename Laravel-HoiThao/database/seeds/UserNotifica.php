<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class UserNotifica extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            DB::table('notificationUser')->insert
            ([
                [ 'content' => 'Hội thảo Khoa Học-Công Nghệ',
                'ngaydienrahoithao' =>Carbon::create('2019', '05', '26'),
                'trangthai'=>false,
                'tenhoithao'=>'abcs',
                'user_id' => 1,
                'seminar_id' => 1]
            ]);
        }
    }
}
