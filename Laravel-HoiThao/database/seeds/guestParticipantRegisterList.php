<?php

use Illuminate\Database\Seeder;

class guestParticipantRegisterList extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('guestParticipantRegisterList')->insert
    	([
    		['name'=>"Lê Quốc Khánh",'adress'=>"28 Nguyễn Tri Phương, Tp. Huế",'email'=>"khanh.le@codegym.vn",'phone'=>"0906438113",'dob'=>"22/2/1990",'seminar_id'=>1]
    		
    	]);
    }
}
