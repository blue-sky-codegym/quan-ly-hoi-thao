import { Component, OnInit } from '@angular/core';
import { Contact } from 'app/Model/Contact.module';
import { ContactService } from 'app/Sevice/Contact/contact.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listcontact',
  templateUrl: './listcontact.component.html',
  styleUrls: ['./listcontact.component.scss']
})
export class ListcontactComponent implements OnInit {
  contacts: Contact[];
  constructor(private contactSevice: ContactService, private router: Router) { }

  ngOnInit() {

    this.contactSevice.getContact().subscribe(
      data => this.contacts = data,
      error => alert("lỗi"))
  }

  deleteContact(id: number) {
    this.contactSevice.deleteContact(id).subscribe(
      next => (
        alert("Bạn đã xóa thành công"),
        this.contacts = this.contacts.filter(next => next.id !== id))
    )
  }
}
