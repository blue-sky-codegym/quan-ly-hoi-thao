import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlideSeminarComponent } from './slide-seminar.component';

describe('SlideSeminarComponent', () => {
  let component: SlideSeminarComponent;
  let fixture: ComponentFixture<SlideSeminarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlideSeminarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlideSeminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
