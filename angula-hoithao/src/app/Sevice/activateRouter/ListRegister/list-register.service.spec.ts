import { TestBed } from '@angular/core/testing';

import { ListRegisterService } from './list-register.service';

describe('ListRegisterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListRegisterService = TestBed.get(ListRegisterService);
    expect(service).toBeTruthy();
  });
});
