import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarAndBannerComponent } from './navbar-and-banner.component';

describe('NavbarAndBannerComponent', () => {
  let component: NavbarAndBannerComponent;
  let fixture: ComponentFixture<NavbarAndBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavbarAndBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarAndBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
