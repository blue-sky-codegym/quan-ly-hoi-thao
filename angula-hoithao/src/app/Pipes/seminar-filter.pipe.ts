import { PipeTransform, Pipe } from '@angular/core';
import { Seminar } from 'app/Model/Seminar.module';

@Pipe({
    name:'seminarFilter'
})
export class SeminarFilterPipe implements PipeTransform{
    transform(seminars: Seminar[], searchTerm: string): Seminar[] {
        if(!seminars|| !searchTerm){
            return seminars;
        }
        return seminars.filter(seminar=> 
            seminar.seminarName.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1);
    }
}