import { PipeTransform, Pipe } from '@angular/core';
import { Seminar } from 'app/Model/Seminar.module';

@Pipe({
    name:'seminarUserFilter'
})
export class SeminarUserFilterPipe implements PipeTransform{
    transform(seminars: Seminar[], userSearching: string): Seminar[] {
        if(!seminars|| !userSearching){
            return seminars;
        }
        return seminars.filter(seminar=> 
            seminar.seminarName.toLowerCase().indexOf(userSearching.toLowerCase()) !== -1);
    }
}