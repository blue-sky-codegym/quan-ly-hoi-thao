import { Component, OnInit } from '@angular/core';
import { Seminar } from 'app/Model/Seminar.module';
import { SeminarService } from 'app/Sevice/Seminar/seminar.service';
import { registerSeminar } from 'app/Model/registerSeminar.module';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegisterService } from 'app/Sevice/RegisterSeminar/register.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { InfomationUserrService } from 'app/Sevice/infomationUse/infomation-userr.service';
import { User } from 'app/user/user';
import { InfomationUser } from 'app/Model/infomationUser.module';
import { AlertService } from 'ngx-alerts';
import { ViewEncapsulation } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-form-register',
  templateUrl: './form-register.component.html',
  styleUrls: ['./form-register.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class FormRegisterComponent implements OnInit {
  public register: registerSeminar;
  public addForm: FormGroup;
  public seminar_id: number;
  public inforSeminar: Seminar;
  public user: any;
  public userRegister = [];
  public userInterest = [];
  public alert: Object;
  public checkInterest: Object;
  public addForm2: FormGroup;
  public user_id: number;
  public informationUser: InfomationUser;
  public userRegisterSponso = [];
  public show:boolean

  constructor(
    private registerService: RegisterService,
    private fb: FormBuilder,
    private routes: ActivatedRoute,
    private seminarService: SeminarService,
    private infomationService: InfomationUserrService,
    private alertService:AlertService
  ) { }

  ngOnInit() {
    $('#alertregis').hide();
    this.addForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(4)]],
      adress: ['', [Validators.required, Validators.minLength(4)]],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required, Validators.minLength(9)]],
      dob: ['', Validators.required],
      content: ['', [Validators.required, Validators.minLength(4)]],
    });

    this.addForm2 = this.fb.group({
      content: ['', [Validators.required, Validators.minLength(4)]],
    });

    this.seminar_id = +this.routes.snapshot.paramMap.get('id');
    this.seminarService.getSeminarByid(this.seminar_id).subscribe(
      data => this.inforSeminar = data);

    this.user = JSON.parse(localStorage.getItem('user'));
    if (this.user) { 
      this.userInterest.push(this.user.id, this.seminar_id); 
      this.registerService.checkInterest(this.userInterest).subscribe(
        data => [this.checkInterest = data,this.userInterest = []]
      )
      if(this.user){
       this.show = true
      }else {
        this.show = false
      }
    }
       this.infomationService.getbyIdUser(this.user.id).subscribe(data=>this.informationUser = data)

  }

  onUserRegister() {
    if (this.user) { this.userRegister.push(this.user.id, this.seminar_id); }  
    this.registerService.userregister(this.userRegister).subscribe(
      data => [this.alert = data, this.userRegister = [],
      $('#alertregis').show(),
      setTimeout(function () { $('#alertregis').hide() }, 3000)]
    )
  }

  onSubmit() {
    this.registerService.addRegister(this.addForm.value, this.seminar_id).subscribe(
      next => (
        this.alertService.info("Cám ơn bạn đã đăng kí tham gia"),
        this.addForm.reset()
      )
    )
  }

  interest() {
    if (this.user) { this.userInterest.push(this.user.id, this.seminar_id); }
    this.registerService.userInterest(this.userInterest).subscribe(
      data => [this.checkInterest = data, this.userInterest = []]
    )
  }

  addSubmit() {
    this.userRegisterSponso.push(this.user.id, this.seminar_id, this.addForm2.value);
    this.registerService.addSponsor(this.userRegisterSponso).subscribe(
      data => [
        this.alertService.info("Cám ơn bạn đã đăng kí tài trợ cho hội thảo"),
        this.userRegisterSponso = [],
        this.addForm2.reset(),        
      ]);
  }
}
