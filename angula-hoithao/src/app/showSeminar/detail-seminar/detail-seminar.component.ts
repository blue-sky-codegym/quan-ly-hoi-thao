import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SeminarService } from 'app/Sevice/Seminar/seminar.service';
import { Seminar } from 'app/Model/Seminar.module';

@Component({
  selector: 'app-detail-seminar',
  templateUrl: './detail-seminar.component.html',
  styleUrls: ['./detail-seminar.component.scss']
})
export class DetailSeminarComponent implements OnInit {

  seminar: Seminar;

  constructor(private router: ActivatedRoute, private seminarSevice: SeminarService) { }

  ngOnInit() {
    const id = +this.router.snapshot.paramMap.get('id');
    this.seminarSevice.getSeminarByid(id).subscribe(
      next => (
        this.seminar = next
      
      )
     
    );
  }

}


