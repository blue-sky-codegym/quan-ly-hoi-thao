import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LbdModule } from '../../lbd/lbd.module';
import { NguiMapModule } from '@ngui/map';
import { HttpClientModule } from '@angular/common/http';

import { AdminLayoutRoutes } from './admin-layout.routing';
import { HomeComponent } from '../../home/home.component';
import { UserComponent } from '../../user/user.component';
import { TablesComponent } from '../../tables/tables.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { AddSeminarComponent } from './../../CRUDseminar/add-seminar/add-seminar.component';
import { EditSeminarComponent } from './../../CRUDseminar/edit-seminar/edit-seminar.component';
import { DetailSeminarComponent } from './../../CRUDseminar/detail-seminar/detail-seminar.component';
import { AddAdsComponent } from './../../CRUDads/add-ads/add-ads.component';
import { EditAdsComponent } from './../../CRUDads/edit-ads/edit-ads.component';
import { ListAdsComponent } from './../../CRUDads/list-ads/list-ads.component';
import { AddCategoryComponent } from './../../CRUDcategory/add-category/add-category.component';
import { EditCategoryComponent } from './../../CRUDcategory/edit-category/edit-category.component';
import { FileUploadModule } from 'ng2-file-upload';
import { CategorySeminarComponent } from 'app/homeComponent/Categorys-Seminar/category-seminar/category-seminar.component';
import { CreateCategoryComponent } from 'app/homeComponent/Categorys-Seminar/create-category/create-category.component';
import { ListUserComponent } from 'app/CRUDuser/list-user/list-user.component';
import { DetailUserComponent } from 'app/CRUDuser/detail-user/detail-user.component';
import { ListcontactComponent } from 'app/CRUDcontact/listcontact/listcontact.component';
import { AddcontactComponent } from 'app/CRUDcontact/addcontact/addcontact.component';
import { EditcontactComponent } from 'app/CRUDcontact/editcontact/editcontact.component';
import { RegisterListComponent } from './../../registerList/register-list/register-list.component';
import { MyListRegistComponent } from './../../userProfile/my-list-regist/my-list-regist.component';
import { ListSponsorComponent } from './../../registerList/list-sponsor/list-sponsor.component';
import { SeminarFilterPipe } from 'app/Pipes/seminar-filter.pipe';
import { categoryFilterPipe } from 'app/Pipes/category-filter.pipe';
import { NgxPaginationModule } from 'ngx-pagination';
import { SendmailComponent } from 'app/sendmail/sendmail/sendmail.component'
import { AlertModule } from 'ngx-alerts';
import { ListFeedbackComponent } from 'app/FeedBack/list-feedback/list-feedback.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    LbdModule,
    HttpClientModule,
    FileUploadModule,
    NguiMapModule.forRoot({ apiUrl: 'https://maps.google.com/maps/api/js?key=YOUR_KEY_HERE' }),
    NgxPaginationModule,
    AlertModule.forRoot({ maxMessages: 5, timeout: 5000, position: 'right' }),

  ],
  declarations: [
    HomeComponent,
    UserComponent,
    TablesComponent,
    TypographyComponent,
    AddCategoryComponent,
    IconsComponent,
    DetailUserComponent,
    MapsComponent,
    EditCategoryComponent,
    AddSeminarComponent,
    ListUserComponent,
    SendmailComponent,
    EditSeminarComponent,
    DetailSeminarComponent,
    NotificationsComponent,
    UpgradeComponent,
    AddAdsComponent,
    EditAdsComponent,
    ListAdsComponent,
    CategorySeminarComponent,
    CreateCategoryComponent,
    EditCategoryComponent,
    ListUserComponent,
    DetailUserComponent,
    ListcontactComponent,
    AddcontactComponent,
    EditcontactComponent,
    RegisterListComponent,
    MyListRegistComponent,
    ListSponsorComponent,
    SeminarFilterPipe,
    categoryFilterPipe,
    ListFeedbackComponent,
    AudilogComponent
  ],
  exports: [
    NgxPaginationModule
  ]
})

export class AdminLayoutModule { }
