import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudilogComponent } from './audilog.component';

describe('AudilogComponent', () => {
  let component: AudilogComponent;
  let fixture: ComponentFixture<AudilogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudilogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudilogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
