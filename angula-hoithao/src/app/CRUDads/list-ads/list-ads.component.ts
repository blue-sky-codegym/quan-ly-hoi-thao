import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdsService } from 'app/Sevice/ads/ads.service';
import { ads } from 'app/Model/ad.module';

@Component({
  selector: 'app-list-ads',
  templateUrl: './list-ads.component.html',
  styleUrls: ['./list-ads.component.scss']
})
export class ListAdsComponent implements OnInit {
  public ads: ads[] = [];
  public ad: ads;
  user:any;
  show:boolean;

  constructor(
    private adsService: AdsService,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.adsService.getAds().subscribe(
      data => (this.ads = data
      ),
      error => alert("Mất kết nối service"));
      this.user = JSON.parse(localStorage.getItem('user'));
      if(this.user.name = "admin"){
          this.show = true       
      }else{
        this.show = false
      }

  }

  deleteSeminar(id) {
    if(confirm("Bạn muốn xóa quảng cáo này")) {
      this.adsService.deleteAd(id).subscribe(
        data => (
          alert("Bạn đã xóa thành công"),
          this.ads = this.ads.filter(t => t.id !== id))
      )
    }
  }

  statusAds(id: number) {
    this.adsService.getAdById(id).subscribe(
      next => {
        this.ad = next;
        if (this.ad.status) {
          this.ad.status = false;
          this.adsService.updateAd(this.ad, id).subscribe(data => {
            this.ngOnInit();
          });
        }
        else {
          this.ad.status = true;
          this.adsService.updateAd(this.ad, id).subscribe(data => {
            this.ngOnInit();
          });
        }
      }
    )
  }
}
