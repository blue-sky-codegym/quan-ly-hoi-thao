import { Component, OnInit } from '@angular/core';
import { AuditLogService } from 'app/Sevice/audit-log.service';
import { auditLog } from 'app/Model/auditLog.module';

@Component({
  selector: 'app-audilog',
  templateUrl: './audilog.component.html',
  styleUrls: ['./audilog.component.scss']
})
export class AudilogComponent implements OnInit {

  public listLog: auditLog[];

  constructor(
    private auditLogService: AuditLogService,
  ) { }

  ngOnInit() {
    this.getAllLogs();
  }

  getAllLogs() {
    this.auditLogService.getAllLogs().subscribe(data => {
      this.listLog = data;
      return this.listLog.sort((a, b) => {
        return <any>new Date(b.created_at) - <any>new Date(a.created_at);
      });
    })
  }

} 
