import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategorySeminarComponent } from './category-seminar.component';

describe('CategorySeminarComponent', () => {
  let component: CategorySeminarComponent;
  let fixture: ComponentFixture<CategorySeminarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategorySeminarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategorySeminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
