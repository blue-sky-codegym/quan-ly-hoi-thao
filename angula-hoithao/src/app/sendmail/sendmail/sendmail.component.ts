import { Component, OnInit } from '@angular/core';
import { SendMailService } from '../../Sevice/sendmail/sendmail.service'

@Component({
  selector: 'app-sendmail',
  templateUrl: './sendmail.component.html',
  styleUrls: ['./sendmail.component.scss']
})
export class SendmailComponent implements OnInit {
  email:String;
  title:String;
  constructor(
    private sendmaiservice:SendMailService
  ) { }

  ngOnInit() {
  }

  sendmail(){
    let query={
      email:this.email,
      title:this.title
    }
    this.sendmaiservice.sendmail(query).subscribe(
      data => {
        alert('Send mail thành công')
        this.email='';
        this.title='';
        },
      error=>{
        alert('Quá trình sendmail bị lổi')
      }
        
      )
      }
}

