import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Feckback } from 'app/Model/Feckback.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  api = 'http://127.0.0.1:8000/api/feedback';

  constructor(private http: HttpClient) { }

  getListFeedback() {
    return this.http.get<Feckback[]>(`${this.api}/list`);
  }

  deleteFeedback(id): Observable<any> {
    return this.http.delete(`${this.api}/delete/${id}`);
  }
}
