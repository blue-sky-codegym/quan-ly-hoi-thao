<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuestparticipantregisterlistTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'guestParticipantRegisterList';

    /**
     * Run the migrations.
     * @table guestParticipantRegisterList
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
      
            $table->increments('id');
            $table->string('name', 191);
            $table->string('adress', 191);
            $table->string('email', 191);
            $table->string('phone', 191);
            $table->string('dob', 191);          
            $table->boolean('closeTheList')->default(false);
            $table->integer('seminar_id')->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
