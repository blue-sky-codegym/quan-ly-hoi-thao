<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegisterroleTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'registerRole';

    /**
     * Run the migrations.
     * @table registerRole
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
         
            $table->increments('id');
            $table->boolean('accessSeminar')->default(false);
            $table->boolean('viewLog')->default(false);
            $table->boolean('editSeminar')->default(false);
            $table->boolean('editBanner')->default(false);
            $table->boolean('editContactInfo')->default(false);
            $table->boolean('EditAds')->default(false);
            $table->boolean('inviteSeminar')->default(false);
            $table->boolean('editParticipateList')->default(false);
            $table->boolean('editUser')->default(false);
            $table->boolean('editCategory')->default(false);
            $table->boolean('editSponsor')->default(false);
            $table->boolean('editComment')->default(false);
            $table->integer('user_id')->unsigned();
            $table->timestamps();
   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
