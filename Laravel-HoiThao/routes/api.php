<?php

// auth
Route::group(['middleware' => 'api'], function ($router) {
    Route::post('login', 'authcontroller@login');
    Route::post('signup', 'authcontroller@signup');
    Route::post('logout', 'authcontroller@logout');
    Route::post('refresh', 'authcontroller@refresh');
    Route::post('me', 'authcontroller@me');
    Route::post('resetPassword','authcontroller@changepassword');
});

// hội thảo
Route::get('seminars/list', 'SeminarController@index');
Route::get('seminars/listseminarnext', 'SeminarController@indexseminarnext');
Route::get('seminars/listdone', 'SeminarController@indexdone');
Route::get('seminars/getSeminar/{id}', 'SeminarController@show');
Route::post('seminars/store', 'SeminarController@store');
Route::put('seminars/update/{id}', 'SeminarController@update');
Route::delete('seminars/delete/{id}', 'SeminarController@delete');
Route::get('seminars/comment/{id}', 'SeminarController@getComment');
Route::put('seminars/comment/edit/{id}', 'SeminarController@updateComment');
Route::delete('seminars/deleteComment/{id}', 'SeminarController@delComment');



// thể loại hội thảo
Route::get('category/list', 'CategoryController@index');
Route::get('category/show/{id}', 'CategoryController@show');
Route::post('category/store', 'CategoryController@store');
Route::put('category/update/{id}', 'CategoryController@update');
Route::delete('category/delete/{id}', 'CategoryController@delete');

// edit quảng cáo
Route::get('ads/list', 'adsController@index');
Route::get('ads/show/{id}', 'adsController@show');
Route::post('ads/store', 'adsController@store');
Route::put('ads/update/{id}', 'adsController@update');
Route::delete('ads/delete/{id}', 'adsController@delete');

// edit user
Route::post('editInformationUser/{id}', 'SeminarController@editInforUser');
Route::post('postComment', 'SeminarController@postComment');
Route::post('editInformationUser/{id}/image','SeminarController@editUserAvatar');


//chi tiết user
Route::get('infomationUser/infomation', 'InfomationUserController@index');
Route::get('infomationUser/showUser/{id}', 'InfomationUserController@show');
Route::delete('infomationUser/delete/{id}', 'InfomationUserController@delete');
Route::put('infomationUser/updatRolers/{id}', 'InfomationUserController@updateRoler');

// upload image cho seminar
Route::post('admin/upload', 'SeminarController@storeImage');

//edit contact
Route::get('contact/list', 'ContactController@index');
Route::get('contact/show/{id}', 'ContactController@show');
Route::post('contact/store', 'ContactController@store');
Route::put('contact/update/{id}', 'ContactController@update');
Route::delete('contact/delete/{id}', 'ContactController@delete');
Route::post('contact/addFeckback', 'ContactController@feedback');


// đăng ký tham gia
Route::post('registerSeminar/store/{id}', 'RegisterSeminarController@store');
Route::post('registerSeminar/userRegister', 'RegisterSeminarController@storeUser');
Route::post('registerSeminar/registerSponsor', 'RegisterSeminarController@Sponsor');
Route::post('registerSeminar/userInterest', 'RegisterSeminarController@userInterest');
Route::post('registerSeminar/checkInterest', 'RegisterSeminarController@checkInterest');

// danh sach tham gia
Route::get('registerSeminar/getListGuest/{id}', 'RegisterSeminarController@getListGuest');
Route::get('registerSeminar/getListUser/{id}', 'RegisterSeminarController@getListUser');
Route::get('registerSeminar/registerSponsorList/{id}', 'RegisterSeminarController@getSponsor');
Route::get('registerSeminar/registerSponsorList/{id}', 'RegisterSeminarController@getSponsor');
Route::get('feedback/list', 'FeedbackController@index');
Route::delete('feedback/delete/{id}', 'FeedbackController@delete');

//danh sách người dùng đã đăng kí hội thảo tham gia và tài trợ
Route::get('registerSeminar/getUserRegisterSeminar/{id}', 'RegisterSeminarController@getSeminarUserRegister');
Route::get('registerSeminar/getSponsorUserRegister/{id}', 'RegisterSeminarController@getSponsorUserRegister');


//notification
Route::post('notification/listnotification','NotificationController@index');
Route::post('notification/updatestatus','NotificationController@updateStatus');

//sendmail
Route::post('sendmail','sendmailController@sendmail');

//Audit Log
Route::get('auditLog/ListLog', 'auditLogController@index');
Route::delete('auditLog/delete', 'auditLogController@delete');

