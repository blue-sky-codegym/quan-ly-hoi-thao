import { TestBed } from '@angular/core/testing';

import { EditSeminarService } from './edit-seminar.service';

describe('EditSeminarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditSeminarService = TestBed.get(EditSeminarService);
    expect(service).toBeTruthy();
  });
});
