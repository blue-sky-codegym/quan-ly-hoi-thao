import { Injectable } from '@angular/core';
import { TokenService } from 'app/authComponent/authSevice/token.service';
import { InfomationUserrService } from 'app/Sevice/infomationUse/infomation-userr.service';
import { Observable } from 'rxjs';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AfterLoginService implements CanActivate {
  public user: any;

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
       if(this.Token.loggedIn()){
           this.user = JSON.parse(localStorage.getItem('user'));
           this.userSevice.getbyIdUser(this.user.id).subscribe(
             data => [
              localStorage.removeItem('user'),
              localStorage.setItem('user', JSON.stringify(data)),
             ]
           )
       }
    return this.Token.loggedIn();
  }
  constructor(private Token: TokenService, private userSevice: InfomationUserrService) { }
}
