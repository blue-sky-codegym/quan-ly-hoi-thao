<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorys extends Model
{
    protected $table='categorys';
    protected $fillable = ['categoryName','description'];
    public function seminars(){
        return $this->hasMany(Seminar::class);
    }
}
