import { Component, OnInit } from '@angular/core';
import { ChangeStatusService } from './../../authComponent/authSevice/change-status.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { TokenService } from '../../authComponent/authSevice/token.service';
import { ViewEncapsulation } from '@angular/core';
import { NotificationService } from '../../Sevice/notificationSeminar/notificationseminar.service';

@Component({
  selector: 'app-navbar-and-banner',
  templateUrl: './navbar-and-banner.component.html',
  styleUrls: ['./navbar-and-banner.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class NavbarAndBannerComponent implements OnInit {

  public loggedIn: boolean;
  public searchText: string;
  public form;
  private user: any;
  private notification: any;
  private notificationarry = [];
  private arridnotifi = [];
  private countnotification: any;
  private resevernotification = [];
  private ngaysapdienrasmn: any;
  private isShownotifi: boolean;
  private isShowcount: boolean;
  private wasInside = false;


  constructor(
    private ChangeStatus: ChangeStatusService,
    private router: Router,
    private Token: TokenService,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
  ) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.ChangeStatus.authStatus.subscribe(value => this.loggedIn = value);
    this.kiemtrahoithao();
    this.isShownotifi = false;
  }

  logout(event: MouseEvent) {
    event.preventDefault();
    this.Token.remove();
    this.ChangeStatus.changeAuthStatus(false);    
    this.router.navigateByUrl('/home');
  }


  kiemtrahoithao() {
    if (this.loggedIn) {
      this.notificationService.getNotification(this.user.id).subscribe(
        data => {
          this.notification = data;
          this.resevernotification = this.notification.reverse();
          this.countnotification = this.notification.length;
          if (this.countnotification == 0) {
            this.isShowcount = false;
          } else {
            this.isShowcount = true;
          }
        },
        error => {
          console.log('error', error)
        }
      )
    }
  }
  searchClick() {
    let navigateData: NavigationExtras = {
      queryParams: {
        "searchText": this.searchText,
      }
    }
    this.router.navigate(["filter-seminar"], navigateData);

  }

  shownotification() {
    this.isShownotifi = !this.isShownotifi;
    this.notificationService.updatestatus(this.user.id).subscribe(
      data => {
        console.log('data', data)
      },
      error => {
        console.log('orror', error)
      }
    )
    this.isShowcount = false;
  }
}
