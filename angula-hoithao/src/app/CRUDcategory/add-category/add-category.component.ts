import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CategoryService } from 'app/Sevice/category/category.service';
import { AlertService } from 'ngx-alerts';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit {

  addFormCategory: FormGroup;

  constructor( private formBuilder: FormBuilder, 
    private router: Router, 
    private categoryService: CategoryService,
    private alertService: AlertService) { }

  ngOnInit() {
    this.addFormCategory = this.formBuilder.group({
      categoryName: ['', [Validators.required, Validators.minLength(5)]],
      description: ['', [Validators.required, Validators.minLength(10)]],
    })
  }

  onSubmit() {
    this.categoryService.addCategory(this.addFormCategory.value)
      .subscribe(data =>{    
        this.alertService.info('Bạn đã thêm thành công'),   
        this.router.navigate(['/admin/table']);
      });
    }

}
