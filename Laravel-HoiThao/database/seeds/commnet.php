<?php

use Illuminate\Database\Seeder;

class commnet extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sermina = App\Seminar::all();

        // Populate the pivot table
        App\User::all()->each(function ($user) use ($sermina) { 
            $user->comment()->attach(
                $sermina->random(rand(1, 3))->pluck('id')->toArray()
            ); 
        });
    }
}
