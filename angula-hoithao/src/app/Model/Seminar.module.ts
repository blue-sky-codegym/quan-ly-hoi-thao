import { Time } from "@angular/common";

export class Seminar {
    public id : number;
    public  seminarName: string;
    public content: any;
    public host: string;
    public image: string;
    public adress: string;
    public heldTime: Date;
    public startTime: Time;

    public registrationTime: string
    public status: boolean;
    public Highlights: boolean;
    public summary: string;
    public capacity: number;
    public categorys_id: number;
}