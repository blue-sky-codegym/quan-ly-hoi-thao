import { Component, OnInit } from '@angular/core';
import { Seminar } from 'app/Model/Seminar.module';
import { Category } from 'app/Model/category';
import { DomSanitizer } from '@angular/platform-browser';
import { SeminarService } from 'app/Sevice/Seminar/seminar.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from 'app/Sevice/category/category.service';
@Component({
  selector: 'app-old-seminar',
  templateUrl: './old-seminar.component.html',
  styleUrls: ['./old-seminar.component.css']
})
export class OldSeminarComponent implements OnInit {
  seminars: Seminar[] = [];
  categorys: Category[] = [];
  public user:any;
  constructor(
    private seminarService: SeminarService,
    private categoryService: CategoryService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.getSeminardone();
  }


  getSeminardone(){
    this.seminarService.getSeminardone().subscribe(
      data => {
        this.seminars = data
        for (var i in this.seminars) {
          this.seminars[i].content = this.seminars[i].content.substr(0, 500) + '...';
          this.seminars[i].content = this.sanitizer.bypassSecurityTrustHtml(this.seminars[i].content);
        }
      },
      error => alert("Mất kết nối service"));
    this.categoryService.getListCategory().subscribe(data => {
      this.categorys = data
    });
  }
}
