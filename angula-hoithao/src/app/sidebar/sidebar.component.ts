import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChangeStatusService } from 'app/authComponent/authSevice/change-status.service';
import { TokenService } from 'app/authComponent/authSevice/token.service';

declare const $: any;
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  // { path: '/admin/dashboard', title: 'Thống kê', icon: 'pe-7s-graph', class: '' },
  { path: '/admin/user', title: 'Thông tin tài khoản', icon: 'pe-7s-user', class: '' },
  { path: '/admin/table', title: 'Danh sách', icon: 'pe-7s-note2', class: '' },
  { path: '/admin/icons', title: 'Quản lý dữ liệu', icon: 'pe-7s-science', class: '' },
  { path: '/goBack', title: 'Quay về trang chủ', icon: 'pe-7s-prev', class: '' },
];

export const ROUTESuser: RouteInfo[] = [
  { path: '/myAccout/user', title: 'Thông tin tài khoản', icon: 'pe-7s-user', class: '' },
  { path: '/myAccout/MyList-RegistSeminar', title: 'Danh sách đăng ký', icon: 'pe-7s-note2', class: '' },
  { path: '/admin/table', title: 'Danh sách', icon: 'pe-7s-note2', class: '' },
  { path: '/admin/icons', title: 'Quản lý dữ liệu', icon: 'pe-7s-science', class: '' },
  { path: '/goBack', title: 'Quay về trang chủ', icon: 'pe-7s-prev', class: '' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  public user:any;
  public ishowaccessadmin:boolean;
  
  constructor(
    private route: ActivatedRoute,
    private ChangeStatus: ChangeStatusService,
    private router: Router,
    private Token: TokenService,) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));  
    if(!this.user.accessAdminPage){
      this.ishowaccessadmin=false;
    } else{
      this.ishowaccessadmin=true;
    }
    if ( this.user.id > 1 ) {
      this.menuItems = ROUTESuser.filter(menuItem => menuItem);
    } else {
      this.menuItems = ROUTES.filter(menuItem => menuItem);
    }
  }
  
  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  };

  logout(event: MouseEvent) {
    event.preventDefault();
    this.Token.remove();
    this.ChangeStatus.changeAuthStatus(false);  
    this.router.navigateByUrl('/home');
  }
}






  // { path: '/admin/typography', title: 'Các danh sách đăng ký',  icon:'pe-7s-news-paper', class: '' },
  // { path: '', title: 'Quay về trang chủ',  icon:'pe-7s-rocket', class: 'active-pro' },
  // { path: '/admin/maps', title: 'map',  icon:'pe-7s-map-marker', class: '' },
  // { path: '/admin/notifications', title: 'Nhật kí',  icon:'pe-7s-bell', class: '' },