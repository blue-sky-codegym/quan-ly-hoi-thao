<?php

namespace App\Http\Controllers;



class sendmailController extends Controller
{
    public function sendmail(\Illuminate\Http\Request $request, \Illuminate\Mail\Mailer $mailer)
    {
        $mailer->to($request->input('email'))
        ->send(new \App\Mail\MyMail($request->input('title')));
        

       return $request;
    }
}
