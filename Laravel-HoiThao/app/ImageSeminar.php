<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageSeminar extends Model
{
    protected $table='imageSeminar';
    public function categorys(){
        return $this->belongsTo(Categorys::class);
    }
}
