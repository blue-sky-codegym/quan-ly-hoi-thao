import { Component, OnInit } from '@angular/core';
import { InfomationUser } from 'app/Model/infomationUser.module';
import { InfomationUserrService } from 'app/Sevice/infomationUse/infomation-userr.service';
import { AlertService } from 'ngx-alerts';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss']
})
export class ListUserComponent implements OnInit {

  users: InfomationUser[];

  constructor(private infomationService:InfomationUserrService,private alertService: AlertService) { }

  ngOnInit() {
    this.infomationService.getInfomation().subscribe(
      data => this.users = data ,
      error => alert(Error));
  }

  deleteUser(id:number){
    if(confirm("Bạn muốn xóa người dùng này")) {
      this.infomationService.deleteUser(id).subscribe(
        next=>(
          this.alertService.info('Bạn đã xóa thành công'),
          this.users = this.users.filter(next=>next.id !== id))
      )
    }
  }

}
