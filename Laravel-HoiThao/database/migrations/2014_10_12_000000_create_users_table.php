<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 191);
            $table->string('email', 191)->unique(); 
            $table->string('adress', 191);
            $table->string('phone', 191);
            $table->string('dob', 191); 
            $table->string('about', 191)->nullable(); 
            $table->string('image', 191)->nullable(); 
            $table->string('password',191);
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->boolean('gender')->nullable();
            $table->boolean('accessAdminPage')->nullable();
            $table->boolean('accessSeminar')->default(false);
            $table->boolean('viewLog')->default(false);
            $table->boolean('editSeminar')->default(false);       
            $table->boolean('editContact')->default(false);       
         
            $table->boolean('inviteSeminar')->default(false);
           
            $table->boolean('editUser')->default(false);
            $table->boolean('addCategory')->default(false);
            $table->boolean('addAds')->default(false);
            $table->boolean('listRegister')->default(false);
            $table->boolean('editSponsor')->default(false);
            $table->boolean('editComment')->default(false); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
