export class registerSeminar{
    'id':number;
    'name':string;
    'adress':string;
    'email':string;
    'phone':string;
    'dob':string;
    'content':string;
    'closeTheList':boolean;
}