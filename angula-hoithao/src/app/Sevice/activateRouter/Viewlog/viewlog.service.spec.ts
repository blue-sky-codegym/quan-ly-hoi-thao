import { TestBed } from '@angular/core/testing';

import { ViewlogService } from './viewlog.service';

describe('ViewlogService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewlogService = TestBed.get(ViewlogService);
    expect(service).toBeTruthy();
  });
});
