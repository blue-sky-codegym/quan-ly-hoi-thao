import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { InfomationUser } from 'app/Model/infomationUser.module';
import { HttpClient } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class InfomationUserrService {
  apiInfomation ="http://127.0.0.1:8000/api/infomationUser"
  constructor(private http:HttpClient) { }

  getInfomation(): Observable<InfomationUser[]>{
    return this.http.get<InfomationUser[]>(`${this.apiInfomation}/infomation`)
  }
  
  getbyIdUser(id:number): Observable<InfomationUser>{
    return this.http.get<InfomationUser>(`${this.apiInfomation}/showUser/${id}`)
  }

  deleteUser(id:number):Observable<any>{
    return this.http.delete(`${this.apiInfomation}/delete/${id}`)
  }

  addRoles(data,id:number){
    return this.http.put(`${this.apiInfomation}/updatRolers/${id}`,data)
  }
}
