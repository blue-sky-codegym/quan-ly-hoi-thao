import { TestBed } from '@angular/core/testing';

import { AddSeminarService } from './add-seminar.service';

describe('AddSeminarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddSeminarService = TestBed.get(AddSeminarService);
    expect(service).toBeTruthy();
  });
});
