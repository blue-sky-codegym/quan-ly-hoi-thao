                                        <?php

use Illuminate\Database\Seeder;

class contact extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contact')->insert
        ([
            ['name' => 'Trường Đại học Phú Xuân','adress_1'=>'28 Nguyễn Tri Phương, Tp.Huế','adress_2'=>'37 Trần Phú, Tp.Huế','email'=>'admin@gmail.com','phone'=>'0914152703'],
        //     ['name' => 'Hội thảo khoa học cấp Quốc gia','adress_1'=>'28 Nguyễn Tri Phương, Tp.Huế','adress_2'=>'37 Trần Phú, Tp.Huế','email'=>'hoang@gmail.com','phone'=>'0914152702'],
        //     ['name' => 'Giáo dục giá trị trong nhà trường','adress_1'=>'Trường Cán bộ Quản lý giáo dục Tp. Hồ Chí Minh','adress_2'=>'Đại học Huế, Tp.Huế','email'=>'long@gmail.com','phone'=>'0914162704'],
        //     ['name' => 'Hội thảo Văn học Thừa Thiên Huế sau đổi mới 1986','adress_1'=>'Khách sạn Duy Tân, Tp.Huế','adress_2'=>'37 Trần Phú, Tp.Huế','email'=>'admin@gmail.com','phone'=>'0914152703'],
        ]);
    }
}
