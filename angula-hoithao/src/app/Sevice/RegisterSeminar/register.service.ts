import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { registerSeminar } from 'app/Model/registerSeminar.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  apiUrl = "http://127.0.0.1:8000/api/registerSeminar"
  constructor(private http: HttpClient) { }

  addRegister(register: registerSeminar, id: number): Observable<registerSeminar> {
    return this.http.post<registerSeminar>(`${this.apiUrl}/store/${id}`, register)
  }

  userregister(data: any) {
    return this.http.post(`${this.apiUrl}/userRegister`, data);
  }
  
  userInterest(data: any) {
    return this.http.post(`${this.apiUrl}/userInterest`, data);
  }

  getListGuest(id: number): Observable<registerSeminar[]> {
    return this.http.get<registerSeminar[]>(`${this.apiUrl}/getListGuest/${id}`)
  }

  checkInterest(data: any) {
    return this.http.post(`${this.apiUrl}/checkInterest`, data);
  }

  addSponsor(data) {
    return this.http.post(`${this.apiUrl}/registerSponsor`, data)
  }

  getListSponsor(id: number): Observable<registerSeminar[]> {
    return this.http.get<registerSeminar[]>(`${this.apiUrl}/registerSponsorList/${id}`)
  }

  getListUser(id: number): Observable<registerSeminar[]> {
    return this.http.get<registerSeminar[]>(`${this.apiUrl}/getListUser/${id}`)
  }

  getUserRegisterSeminar(id){
    return this.http.get(`${this.apiUrl}/getUserRegisterSeminar/${id}`)
  }

  getSponsorUserRegister(id){
    return this.http.get(`${this.apiUrl}/getSponsorUserRegister/${id}`)
  }
}
