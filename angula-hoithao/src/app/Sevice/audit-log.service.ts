import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { auditLog } from 'app/Model/auditLog.module';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuditLogService {

  api = 'http://localhost:8000/api/auditLog';

  constructor(private http: HttpClient) {}

  getAllLogs(): Observable < auditLog[] > {
    return this.http.get<auditLog[]>(`${this.api}/ListLog`);
  }

  deleteAllLogs() {
    return this.http.delete(`${this.api}/delete`)
  }
}
