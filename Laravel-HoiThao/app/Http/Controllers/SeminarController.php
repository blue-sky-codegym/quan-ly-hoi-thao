<?php

namespace App\Http\Controllers;

use App\Seminar;
use App\Categorys;
use Illuminate\Http\Request;
use App\User;
use App\Comment;

class SeminarController extends Controller
{
    public function index()
    {
        $seminars = Seminar::all();
        foreach ($seminars as $seminar) {
            $seminar->namecategory = $seminar->categorys->categoryName;
        }
        return $seminars;
    }
    
    public function indexseminarnext()
    {
        // $seminars = Seminar::all();
        // foreach ($seminars as $seminar) {
        //     $seminar->namecategory = $seminar->categorys->categoryName;
        // }

        $today=date("Y/m/d");
        $dateend=date('Y/m/d', strtotime(' +2 day'));
        // $expiry = (new DateTime($toBeComparedDate))->format('Y-m-d');
        $seminars = Seminar::where('heldTime','>',$today)->where('heldTime','<',$dateend)->get();
        foreach ($seminars as $seminar) {
            $seminar->namecategory = $seminar->categorys->categoryName;
        }
        return $seminars;
    }
    public function indexdone()
    {
        $date=date("Y/m/d");
        $seminars = Seminar::where('heldTime','<',$date)->get();
        foreach( $seminars as $seminar){
            $seminar->namecategory=$seminar->categorys->categoryName;
        }
       return $seminars;
    }

    public function show($id)
    {
        $seminar = Seminar::find($id);
        return $seminar;
    }


    public function store(Request $request)
    {
        $seminar = new Seminar();
        $seminar->seminarName = $request->input('seminarName');
        $seminar->content = $request->input('content');
        $seminar->host = $request->input('host');
        $seminar->adress = $request->input('adress');
        $seminar->heldTime = $request->input('heldTime');
        $seminar->startTime = $request->input('startTime');
        $seminar->registrationTime = $request->input('registrationTime');
        $seminar->capacity = $request->input('capacity');
        $seminar->categorys_id = $request->input('categorys_id');

        $image = $request->input('image');
        $image = str_replace('data:image/jpeg;base64,', '', $image);
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10) . '.' . 'png';
        $seminar->image = asset('storage/image/' . $imageName);
        \File::put(storage_path() . '/app/public/image/' . $imageName, base64_decode($image));
        $seminar->save();
        return $seminar;
    }

    public function update(Request $request, $id)
    {
        $seminar = Seminar::findOrFail($id);
        $seminar->seminarName = $request->input('seminarName');
        $seminar->content = $request->input('content');
        $seminar->host = $request->input('host');
        $seminar->adress = $request->input('adress');
        $seminar->heldTime = $request->input('heldTime');
        $seminar->startTime = $request->input('startTime');
        $seminar->registrationTime = $request->input('registrationTime');
        $seminar->Highlights = $request->input('Highlights');
        $seminar->capacity = $request->input('capacity');
        $seminar->categorys_id = $request->input('categorys_id');

        $image = $request->input('image');
        if (stripos($image, '127.0.0.1:8000/storage/image') >= 0) {
            $seminar->image = $image;
        } else {
            $image = str_replace('data:image/jpeg;base64,', '', $image);
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10) . '.' . 'png';
            $seminar->image = asset('storage/image/' . $imageName);
            \File::put(storage_path() . '/app/public/image/' . $imageName, base64_decode($image));
        }

        $seminar->save();
        return response()->json($seminar, 200);
    }


    public function delete($id)
    {
        $seminar = Seminar::find($id);
        $seminar->delete();
    }

    public function editInforUser(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->adress = $request->adress;
        $user->dob = $request->dob;
        $user->gender = $request->gender;
        $user->about = $request->about;
        $user->phone = $request->phone;

        $image = $request->input('image');
        if (strlen($image) > 400) {
            $image = str_replace('data:image/jpeg;base64,', '', $image);
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10) . '.' . 'png';
            $user->image = asset('storage/image/' . $imageName);
            \File::put(storage_path() . '/app/public/image/' . $imageName, base64_decode($image));
        } else {
            $user->image = $request->input('image');
        }


        $user->update();

        return $user;
    }

    public function storeImage(Request $request)
    {
        $file = $request->file('file');
        $path = url('/app/public/image/') . '/' . $file->getClientOriginalName();
        $imgpath = $file->move(public_path('/app/public/image/'), $file->getClientOriginalName());
        $fileNameToStore = $path;
        return json_encode(['location' => $fileNameToStore]);
    }

    public function postComment(Request $request)
    {
        $user = User::find($request->input('user_id'));
        $seminar = Seminar::find($request->input('seminar_id'));
        $username = $user->name;
        $comment = $request->only('comment');
        $comment = $comment['comment'];
        $user->comment()->attach($seminar, ['comment' => $comment, 'userName' => $username]);
        $lastID = Comment::all()->last()->id;
        $avatar = $user->image;
        $dataReturn = array("userName" => $username, "comment" => $comment, 'id' => $lastID, "avatarUser" => $avatar);
        return $dataReturn;
    }

    public function getComment($id)
    {
        // $user_id = Comment::where('seminar_id',$id)->get('user_id');
        // $user_avatar = User::where('id',$user_id)->get('image');
        $comments = Comment::where('seminar_id', $id)->orderBy('id', 'desc')->get();
        foreach ($comments as $comment) {
            $comment->avatarUser = User::where('id', $comment->user_id)->get("image");
            $comment->avatarUser =  $comment->avatarUser['0']->image;
        }
        return $comments;
    }

    public function updateComment(request $request, $id)
    {
        $comment = Comment::findOrFail($id);
        $comment->comment = $request->comment;
        $comment->save();
        return $request;
    }

    public function delComment($id)
    {
        $comment = Comment::find($id);
        $comment->delete();
    }
}
