import { Component, OnInit } from '@angular/core';
import { RegisterService } from 'app/Sevice/RegisterSeminar/register.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list-sponsor',
  templateUrl: './list-sponsor.component.html',
  styleUrls: ['./list-sponsor.component.scss']
})
export class ListSponsorComponent implements OnInit {
  ListSponsor =[];
  seminar_id: number;
  constructor(private registerService: RegisterService, private routes: ActivatedRoute) { }

  ngOnInit() {
    this.seminar_id = +this.routes.snapshot.paramMap.get('id');
    this.registerService.getListSponsor(this.seminar_id).subscribe(data => 
      this.ListSponsor = data
    )}

}
