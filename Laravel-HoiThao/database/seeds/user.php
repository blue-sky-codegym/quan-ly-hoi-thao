<?php

use Illuminate\Database\Seeder;

class user extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert
        ([
            [ 'name' => 'admin','email'=>'tranthenhu150@yahoo.com','adress' => '28 Nguyễn Tri Phương, Tp.Huế','phone' => '0914152703','dob'=> '22/2/1997','password'=> bcrypt('111111p'),'accessAdminPage'=>true,'accessSeminar'=>true,'viewLog'=>true,'editSeminar'=>true,'inviteSeminar'=>true,'editUser'=>true,'addCategory'=>true,'editSponsor'=>true,'editComment'=>true,'editContact'=>true,'addAds'=>true,'listRegister'=>true],
            [ 'name' => 'Nguyễn Thế Hoàng','email'=>'hoang@gmail.com','adress' => '28 Nguyễn Tri Phương, Tp.Huế','phone' => '0914152702','dob'=> '12/2/1997','password'=> bcrypt('1234567'),'accessAdminPage'=>false,'accessSeminar'=>false,'viewLog'=>false,'editSeminar'=>false,'inviteSeminar'=>false,'editUser'=>false,'addCategory'=>false,'editSponsor'=>false,'editComment'=>false,'editContact'=>false,'addAds'=>false,'listRegister'=>false],
            [ 'name' => 'Trần Mạnh Long','email'=>'long@gmail.com','adress' => '28 Nguyễn Tri Phương, Tp.Huế','phone' => '0914152704','dob'=> '22/2/1985','password'=> bcrypt('1234567'),'accessAdminPage'=>false,'accessSeminar'=>false,'viewLog'=>false,'editSeminar'=>false,'inviteSeminar'=>false,'editUser'=>false,'addCategory'=>false,'editSponsor'=>false,'editComment'=>false,'editContact'=>false,'addAds'=>false,'listRegister'=>false],
            [ 'name' => 'Hoàng Anh Dũng','email'=>'dung@gmail.com','adress' => '28 Nguyễn Tri Phương, Tp.Huế','phone' => '0914152705','dob'=> '22/2/1996','password'=> bcrypt('1234567'),'accessAdminPage'=>false,'accessSeminar'=>false,'viewLog'=>false,'editSeminar'=>false,'inviteSeminar'=>false,'editUser'=>false,'addCategory'=>false,'editSponsor'=>false,'editComment'=>false,'editContact'=>false,'addAds'=>false,'listRegister'=>false],
            [ 'name' => 'Nguyễn Chí Thanh','email'=>'thanh@gmail.com','adress' => '28 Nguyễn Tri Phương, Tp.Huế','phone' => '0914152706','dob'=> '22/2/1995','password'=> bcrypt('1234567'),'accessAdminPage'=>false,'accessSeminar'=>false,'viewLog'=>false,'editSeminar'=>false,'inviteSeminar'=>false,'editUser'=>false,'addCategory'=>false,'editSponsor'=>false,'editComment'=>false,'editContact'=>false,'addAds'=>false,'listRegister'=>false]
        ]);
    }
}
