import { TestBed } from '@angular/core/testing';

import { EditSponsorService } from './edit-sponsor.service';

describe('EditSponsorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditSponsorService = TestBed.get(EditSponsorService);
    expect(service).toBeTruthy();
  });
});
