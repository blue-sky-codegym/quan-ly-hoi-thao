import { Component, OnInit } from '@angular/core';
import { Seminar } from 'app/Model/Seminar.module';
import { SeminarService } from 'app/Sevice/Seminar/seminar.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CategoryService } from 'app/Sevice/category/category.service';
import { Category } from 'app/Model/category';
import { AlertService } from 'ngx-alerts';


declare var $: any;
declare var jQuery: any;
declare var tinymce: any;

@Component({
  selector: 'app-add-seminar',
  templateUrl: './add-seminar.component.html',
  styleUrls: ['./add-seminar.component.scss']
})

export class AddSeminarComponent implements OnInit {

  category: Category[];
  seminar: Seminar[];
  addForm: FormGroup;

  constructor(private seminarService: SeminarService,
    private categoryService: CategoryService,
    private fb: FormBuilder,
    private router: Router,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    jQuery.getScript("../../assets/tinyMCE.js");
    tinymce.remove();
    this.categoryService.getListCategory().subscribe(data => {
      this.category = data;
    });

    this.addForm = this.fb.group({
      seminarName: ['', [Validators.required, Validators.minLength(4)]],
      content: [''],
      host: ['', [Validators.required, Validators.minLength(3)]],
      adress: ['', [Validators.required, Validators.minLength(3)]],
      heldTime: ['', [Validators.required]],
      startTime: ['', [Validators.required]],
      image: ['', [Validators.required]],
      registrationTime: ['', [Validators.required]],
      capacity: ['', [Validators.required, Validators.min(10)]],
      categorys_id: ['', [Validators.required]],
    }, { validator: this.dateLessThan('registrationTime', 'heldTime') });
  }

  onSubmit() {
    this.addForm.value.content = $('#subValue').val();
    this.seminarService.addSeminar(this.addForm.value).subscribe(
      next => (
        this.alertService.success('Bạn đã thêm thành công'),
        this.addForm.reset(),
        this.router.navigate(['/admin/table'])
      )
    )
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader: any = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        this.addForm.value.image = event.target.result;
      }
    }
  }

  dateLessThan(from: string, to: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let f = group.controls[from];
      let t = group.controls[to];
      if (f.value > t.value) {
        return {
          dates: "Ngày đăng ký phải sớm hơn ngày bắt đầu"
        };
      }
      return {};
    }
  }


}
