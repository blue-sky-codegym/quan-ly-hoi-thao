import { Component, OnInit } from '@angular/core';
import { Category } from 'app/Model/category';
import { Router } from '@angular/router';
import { CategoryService } from 'app/Sevice/category.service';

@Component({
  selector: 'app-category-seminar',
  templateUrl: '././category-seminar.component.html',
  styleUrls: ['././category-seminar.component.scss']
})
export class CategorySeminarComponent implements OnInit {
  categorys: Category[] = [];
  user:any;
  show:boolean;
  constructor(private router: Router, private categoryService: CategoryService) { }

  ngOnInit() {
    this.categoryService.getListCategory().subscribe(data => {
      this.categorys = data
    });
    this.user = JSON.parse(localStorage.getItem('user'));
    if(this.user.name = "admin"){
        this.show = true       
    }else{
      this.show = false
    }
  }

  deleteCategory(id: number){
    this.categoryService.deleteCategory(id).subscribe(
      next=>(
        alert("Bạn đã xóa thành công"),
        this.categorys = this.categorys.filter(next=>next.id !== id))
    )
  }

}
