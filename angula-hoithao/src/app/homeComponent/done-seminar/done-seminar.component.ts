import { Component, OnInit } from '@angular/core';
import { Seminar } from 'app/Model/Seminar.module';
import { SeminarService } from 'app/Sevice/Seminar/seminar.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from 'app/Sevice/category/category.service';
import { Category } from 'app/Model/category';
import { ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-done-seminar',
  templateUrl: './done-seminar.component.html',
  styleUrls: ['./done-seminar.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DoneSeminarComponent implements OnInit {

  seminars: Seminar[] = [];
  categorys: Category[] = [];

  constructor(
    private seminarService: SeminarService,
    private categoryService: CategoryService,
    private sanitizer: DomSanitizer
     ) { }

  ngOnInit() {
    this.seminarService.getSeminardone().subscribe(
      data => {
        this.seminars = data
        for (var i in this.seminars) {
          this.seminars[i].content = this.seminars[i].content.substr(0, 500) + '...';
          this.seminars[i].content = this.sanitizer.bypassSecurityTrustHtml(this.seminars[i].content);
        }
      },
      error => alert("Mất kết nối service"));
    this.categoryService.getListCategory().subscribe(data => {
      this.categorys = data
    });
  }

}
