export class auditLog {
    public id: number;
    public event: string;
    public auditable_type: string;
    public old_values: string;
    public new_values: string;
    public url: string;
    public ip_address: number;
    public created_at: Date;
}
