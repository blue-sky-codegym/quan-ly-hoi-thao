import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenService } from 'app/authComponent/authSevice/token.service';

@Injectable({
  providedIn: 'root'
})
export class AddSeminarService implements CanActivate{
  user: any;
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    this.user = JSON.parse(localStorage.getItem('user'));
    if(this.user.accessSeminar ==true ){     
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
  constructor(private Token: TokenService, private router:Router) { }
  
}
