import { Component, OnInit } from '@angular/core';
import { Seminar } from 'app/Model/Seminar.module';
import { SeminarService } from 'app/Sevice/Seminar/seminar.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from 'app/Sevice/category/category.service';
import { Category } from 'app/Model/category';
import { AlertService } from 'ngx-alerts';


@Component({
    selector: 'app-tables',
    templateUrl: './tables.component.html',
    styleUrls: ['./tables.component.css']
})
export class TablesComponent implements OnInit {
    seminars: Seminar[] = [];
    categorys: Category[] = [];
    searchTerm: string;
    searchCategory: string;
    public c: number = 1;
    user:any;
    show:boolean

    constructor(
        private seminarService: SeminarService,
        private categoryService: CategoryService,
        private alertService: AlertService,

    ) { }

    ngOnInit() {

        this.seminarService.getSeminar().subscribe(
            data => this.seminars = data,
            error => alert("Mất kết nối service"));
        this.categoryService.getListCategory().subscribe(data => {
            this.categorys = data
        });

        this.user = JSON.parse(localStorage.getItem('user'));
        if(this.user.name = "admin"){
            this.show = true       
        }else{
          this.show=false
        }
    }

    deleteSeminar(id) {
        if (confirm("Bạn muốn xóa hội thảo này")) {
            this.seminarService.deleteSeminar(id).subscribe(
                data => (
                    this.alertService.success('Bạn đã xóa thành công'),
                    this.seminars = this.seminars.filter(t => t.id !== id))
            )
        }
    }

    deleteCategory(id: number) {
        if (confirm("Bạn muốn xóa thể loại này")) {
            this.categoryService.deleteCategory(id).subscribe(
                next => (
                    this.alertService.success('Bạn đã xóa thành công'),
                    this.categorys = this.categorys.filter(next => next.id !== id))
            )
        }
    }
}






