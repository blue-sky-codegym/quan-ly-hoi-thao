import { Component, OnInit } from '@angular/core';
import { Contact } from 'app/Model/Contact.module';
import { ContactService } from 'app/Sevice/Contact/contact.service';
import { ViewEncapsulation } from '@angular/core';
@Component({
  selector: 'app-footer-home',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class FooterComponent implements OnInit {

  contacts: Contact[] = [];
  constructor(private contactService: ContactService) { }

  ngOnInit() {
  
    this.contactService.getContact().subscribe( data =>this.contacts = data)
  }

}
