import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { ads } from 'app/Model/ad.module';


@Injectable({
  providedIn: 'root'
})
export class AdsService {
  apiUrl = "http://127.0.0.1:8000/api/ads";
  public ad: ads[];

  constructor(private http: HttpClient) { }
  
  getAds(): Observable<ads[]> {
    return this.http.get<ads[]>(`${this.apiUrl}/list`);
  }
  addAd(ad) {
   
    return this.http.post(`${this.apiUrl}/store`, ad)
  }

  deleteAd(id): Observable<any> {
    return this.http.delete(`${this.apiUrl}/delete/${id}`)
  }
  getAdById(id: number): Observable<ads> {
    return this.http.get<ads>(`${this.apiUrl}/show/${id}`)
  }
  updateAd(ad: ads, id: number): Observable<ads> {
    return this.http.put<ads>(`${this.apiUrl}/update/${id}`, ad)
  }
}
