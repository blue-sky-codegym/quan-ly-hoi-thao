import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CategoryService } from 'app/Sevice/category.service';

@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.scss']
})
export class CreateCategoryComponent implements OnInit {
  addFormCategory: FormGroup;

  constructor( private formBuilder: FormBuilder, private router: Router, private categoryService: CategoryService) { }

  ngOnInit() {
    this.addFormCategory = this.formBuilder.group({
      categoryName: ['', [Validators.required, Validators.minLength(5)]],
      description: ['', [Validators.required, Validators.minLength(10)]],
    })
  }

  onSubmit() {
    this.categoryService.addCategory(this.addFormCategory.value)
      .subscribe(data =>{
        this.router.navigate(['/admin/table']);
      });
    }
}
