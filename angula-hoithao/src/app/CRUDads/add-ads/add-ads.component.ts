import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ads } from 'app/Model/ad.module';
import { AdsService } from 'app/Sevice/ads/ads.service';
import { AlertService } from 'ngx-alerts';

@Component({
  selector: 'app-add-ads',
  templateUrl: './add-ads.component.html',
  styleUrls: ['./add-ads.component.scss']
})
export class AddAdsComponent implements OnInit {
  addAdsForm: FormGroup;
  ad: ads;

  constructor(
    private adService: AdsService,
    private fb: FormBuilder,
    private router: Router,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    const reg = '(https?://)([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
    this.addAdsForm = this.fb.group({
      name: ['', [Validators.required]],
      content: ['', [Validators.required]],
      image: [''],
      URL: ['', [Validators.required, Validators.pattern(reg)]],
    });
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader: any = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        this.addAdsForm.value.image = event.target.result;
      }
    }
  }


  onSubmit() {
    this.adService.addAd(this.addAdsForm.value).subscribe(
      next => (
        this.alertService.success('Bạn đã thêm thành công'),
        this.addAdsForm.reset(),
        this.router.navigate(['/admin/listAds'])
      ))
  }


}
