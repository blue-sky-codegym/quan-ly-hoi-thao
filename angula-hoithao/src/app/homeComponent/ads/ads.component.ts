import { Component, OnInit } from '@angular/core';
import { AdsService } from 'app/Sevice/ads/ads.service';
import { ads } from 'app/Model/ad.module';


@Component({
  selector: 'app-ads',
  templateUrl: './ads.component.html',
  styleUrls: ['./ads.component.css']
})
export class AdsComponent implements OnInit {

  public ads: ads[]=[];
  

  constructor(
    private adsService: AdsService
  ) { }

  ngOnInit() {
    this.getAllAds();
  }


  getAllAds(){
    this.adsService.getAds().subscribe(
      data => (this.ads = data
          ),
      error => alert("Mất kết nối service"));

  }
  
}
