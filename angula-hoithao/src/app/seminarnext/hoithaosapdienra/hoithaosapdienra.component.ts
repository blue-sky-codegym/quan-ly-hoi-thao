import { Component, OnInit } from '@angular/core';
import { SeminarService } from 'app/Sevice/Seminar/seminar.service';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-hoithaosapdienra',
  templateUrl: './hoithaosapdienra.component.html',
  styleUrls: ['./hoithaosapdienra.component.scss']
})
export class HoithaosapdienraComponent implements OnInit {
  public seminars:any=[];
  constructor(
    private seminarService:SeminarService,
    private sanitizer: DomSanitizer
  ) { 
    
  }

  ngOnInit() {
    this.getseminarnext();
  }

  getseminarnext(){
    this.seminarService.getSeminarnext().subscribe(
      data=>{
        console.log('ok',data)
        this.seminars=data;
        for (var i in this.seminars) {
          this.seminars[i].content = this.seminars[i].content.substr(0, 500) + '...';
          this.seminars[i].content = this.sanitizer.bypassSecurityTrustHtml( this.seminars[i].content);
        };
      },
      error=>{

      }
    )
  }

}
