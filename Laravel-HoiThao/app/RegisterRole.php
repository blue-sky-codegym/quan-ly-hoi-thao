<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisterRole extends Model
{
    protected $table='registerRole';
    public function users(){
        return $this->belongsTo(User::class);
    }
}
