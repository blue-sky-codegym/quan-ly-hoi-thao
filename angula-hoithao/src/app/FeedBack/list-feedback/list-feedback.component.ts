import { Component, OnInit } from '@angular/core';
import { Feckback } from 'app/Model/Feckback.model';
import { FeedbackService } from 'app/Sevice/Feedback/feedback.service';
import { AlertService } from 'ngx-alerts';

@Component({
  selector: 'app-list-feedback',
  templateUrl: './list-feedback.component.html',
  styleUrls: ['./list-feedback.component.scss']
})
export class ListFeedbackComponent implements OnInit {
  feedbacks: Feckback[];

  constructor(private feedbackService: FeedbackService, private alertService: AlertService, ) { }

  ngOnInit() {
    this.feedbackService.getListFeedback().subscribe(data => {
      this.feedbacks = data
    })
  }

  deleteFeedback(id: number) {
    if (confirm("Bạn muốn xóa nội dung này")) {
      this.feedbackService.deleteFeedback(id).subscribe(
        next => (
          this.alertService.success('Bạn đã xóa thành công'),
          this.feedbacks = this.feedbacks.filter(next => next.id !== id))
      )
    }
  }
}


