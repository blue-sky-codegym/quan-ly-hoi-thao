import { PipeTransform, Pipe } from '@angular/core';
import { Category } from 'app/Model/category';

@Pipe({
    name:'categoryFilter'
})
export class categoryFilterPipe implements PipeTransform{
    transform(category: Category[] , searchCategory: string): Category[] {
        if(!category|| !searchCategory){
            return category;
        }
        return category.filter(category=> 
            category.categoryName.toLowerCase().indexOf(searchCategory.toLowerCase()) !== -1);
    }
}