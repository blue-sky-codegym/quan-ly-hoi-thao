<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeminarsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'Seminars';

    /**
     * Run the migrations.
     * @table Seminars
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {

            $table->increments('id');
            $table->string('seminarName', 191);
            $table->text('content')->nullable();
            $table->string('host', 191)->nullable();
            $table->string('adress', 191)->nullable();
            $table->date('heldTime', 191)->format('d-m-Y')->nullable();
            $table->time('startTime')->nullable();
            $table->date('registrationTime', 191)->format('d-m-Y')->nullable();
            $table->string('image', 500)->nullable();
            $table->boolean('status')->default(false)->nullable();
            $table->boolean('Highlights')->default(false)->nullable();
            $table->string('summary', 1000)->nullable();
            $table->integer('capacity')->nullable();
            $table->integer('categorys_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
