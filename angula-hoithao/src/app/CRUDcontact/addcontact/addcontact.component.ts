import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ContactService } from 'app/Sevice/Contact/contact.service';
import { Router } from '@angular/router';
import { AlertService } from 'ngx-alerts';

@Component({
  selector: 'app-addcontact',
  templateUrl: './addcontact.component.html',
  styleUrls: ['./addcontact.component.scss']
})
export class AddcontactComponent implements OnInit {
  
  addForm: FormGroup;
  constructor(private contactSevice: ContactService,
    private fb: FormBuilder,
    private router: Router,
    private alertService: AlertService
    ) { }

  ngOnInit() {

    this.addForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(4)]],
      adress_1: ['', [Validators.required, Validators.minLength(3)]],
      adress_2: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.minLength(3)]],
      phone: ['', [Validators.required, Validators.minLength(3)]],
    });
  }
  onSubmit() {    
    this.contactSevice.addContact(this.addForm.value).subscribe(
      next => (
        this.alertService.success('Bạn đã thêm thành công'),
        this.addForm.reset(),
        this.router.navigate(['/admin/listContact'])
      )
    )
  }
}
