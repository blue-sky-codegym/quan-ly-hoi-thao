import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthSeviceService } from './../../authComponent/authSevice/auth-sevice.service';
import { TokenService } from './../../authComponent/authSevice/token.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  public form = {
    email: null,
    name: null,
    password: null,
    password_confirmation: null,
    adress:null,
    phone:null,
    dob:null,
     };
  public error = [];

  constructor(
    private Token: TokenService,
    private  AuthSevice: AuthSeviceService,
    private router: Router
  ) { }

  onSubmit() {
    this.AuthSevice.signup(this.form).subscribe(
      data => [alert('Đăng kí thành công') , 
      this.router.navigateByUrl('login')],
      error => this.handleError(error)
    );
  }

  handleError(error) {
    this.error = error.error.errors;
  }

  ngOnInit() {
  }

}
