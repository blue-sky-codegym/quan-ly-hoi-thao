import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NextSeminarComponent } from './next-seminar.component';

describe('NextSeminarComponent', () => {
  let component: NextSeminarComponent;
  let fixture: ComponentFixture<NextSeminarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NextSeminarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NextSeminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
