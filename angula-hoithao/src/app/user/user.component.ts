import { Component, OnInit } from '@angular/core';
import { UserService } from 'app/Sevice/userSevice/user.service';
import { AuthSeviceService } from '../authComponent/authSevice/auth-sevice.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { TokenService } from '../authComponent/authSevice/token.service';
import { ChangeStatusService } from '../authComponent/authSevice/change-status.service';


declare var $: any;
declare var jQuery: any;
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent implements OnInit {
  
  public user: any;
  public url = '';
  public form;
  public imageForm;
  public oldpw:any;
  public newpw:any;
  public selectednam:any;
  public selectednu:any;
  constructor( 
    private usersSevice: UserService,
    private AuthSevice: AuthSeviceService,
    private router: Router,
    private Token: TokenService,
    private route: ActivatedRoute,
    private ChangeStatus: ChangeStatusService,
 
    ) { }


  ngOnInit() {
    jQuery.getScript("../assets/js/runjs.js");
    this.user = JSON.parse(localStorage.getItem('user'));
    this.form = {
      email: this.user.email,
      name: this.user.name,
      adress: this.user.adress,
      dob: this.user.dob,
      phone: this.user.phone,
      gender: this.user.gender,
      about: this.user.about,
      image: this.user.image
    };
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader: any = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        this.form.image = event.target.result;
      }
    }
  }

  onSubmit() {
    
    if(this.form.gender==1){
      this.selectednam='selected'
    }else{
      this.selectednu='selected'
    }
    this.usersSevice.updateUser(this.form, this.user.id).subscribe(
      data =>
        [
          localStorage.removeItem('user'),
          localStorage.setItem('user', JSON.stringify(data)),
          this.ngOnInit()
        ])
  }

  changepw(){
    let query={
      email:this.user.email,
      password:this.oldpw,
      newpw:this.newpw,
      id:this.user.id
    }  
    this.AuthSevice.changePassword(query).subscribe(
      data => {     
        alert('Cập nhật mật khẩu thành công');
        this.Token.remove();
        this.ChangeStatus.changeAuthStatus(false);
        this.router.navigateByUrl('/login');
      },
      error =>{
        alert('Mật khẩu cũ không đúng')
      }
    );
  }

}
