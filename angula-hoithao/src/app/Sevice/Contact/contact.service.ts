import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Contact } from 'app/Model/Contact.module';
import { Feckback } from 'app/Model/Feckback.model';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  apiContact = "http://127.0.0.1:8000/api/contact"
  constructor(private http:HttpClient) { }
  
  getContact():Observable<Contact[]>{
    return this.http.get<Contact[]>(`${this.apiContact}/list`)
  }

  addContact(contact:Contact): Observable<Contact>{
    return this.http.post<Contact>(`${this.apiContact}/store`,contact);
  }

  getbyIdContact(id:number): Observable<Contact>{
    return this.http.get<Contact>(`${this.apiContact}/show/${id}`)
  }

  updateContact(newcontact:Contact, id:number): Observable<Contact>{
    return this.http.put<Contact>(`${this.apiContact}/update/${id}`,newcontact)
  }

  deleteContact(id:number):Observable<any>{
    return this.http.delete<any>(`${this.apiContact}/delete/${id}`)
  }

  addFeckback(feak:Feckback){
    return this.http.post(`${this.apiContact}/addFeckback`,feak);
  }
}
