import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Category } from 'app/Model/category';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  api = 'http://127.0.0.1:8000/api/category';

  constructor( private http: HttpClient) { }

  getListCategory(){
    return this.http.get<Category[]>(`${this.api}/list`);
  }

  addCategory(category: Category): Observable<Category> {
    return this.http.post<Category>(`${this.api}/store`, category)
  }

  deleteCategory(id): Observable<any>  {
    return this.http.delete(`${this.api}/delete/${id}`);
  }

  getCategoryById(id: number){
    return this.http.get<Category>(`${this.api}/show/${id}`);
  }

  updateCategory(newCategory: Category, id: number){
 
    return this.http.put<Category>(`${this.api}/update/${id}`,newCategory);
  }
}
