<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;


class InfomationUserController extends Controller
{
    public function index()
    {
        $user = User::all();
        return $user;
    }
    public function show($id)
    { 
        $user = User::find($id);
        return $user;
        
    }
    public function delete($id){
        $user = User::find($id);
        $user->delete();
    }

    public function updateRoler(Request $request, $id){        
        $user = User::find($id);  
        $user->accessAdminPage=$request->accessAdminPage;
        $user->listRegister=$request->listRegister;
        $user->addCategory=$request->addCategory;
        $user->addAds=$request->addAds;    
        $user->editContact=$request->editContact;            
        $user->save();
        $user->update($request->all());
        return $user;
    }
    
}
