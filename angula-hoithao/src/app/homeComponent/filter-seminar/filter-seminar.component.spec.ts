import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterSeminarComponent } from './filter-seminar.component';

describe('FilterSeminarComponent', () => {
  let component: FilterSeminarComponent;
  let fixture: ComponentFixture<FilterSeminarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterSeminarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterSeminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
