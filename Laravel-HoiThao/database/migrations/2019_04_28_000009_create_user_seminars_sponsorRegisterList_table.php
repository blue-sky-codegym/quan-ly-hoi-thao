<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSeminarsSponsorregisterlistTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'user_seminars_sponsorRegisterList';

    /**
     * Run the migrations.
     * @table user_seminars_sponsorRegisterList
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {

            $table->increments('id');
            $table->string('content', 500);
            $table->integer('user_id')->unsigned();
            $table->integer('seminar_id')->unsigned();
            $table->boolean('closeTheList')->default(false);
            $table->timestamps();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
