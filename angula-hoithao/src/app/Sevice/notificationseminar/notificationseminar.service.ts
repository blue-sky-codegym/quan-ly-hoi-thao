import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Seminar } from 'app/Model/Seminar.module';

@Injectable({
  providedIn: 'root'
})

export class NotificationService {

  apiUrl = "http://127.0.0.1:8000/api/notification";

  constructor(private http: HttpClient) { }

  getNotification(user_id): Observable<any> {
    return this.http.post(`${this.apiUrl}/listnotification`,user_id);
  }
  updatestatus(array): Observable<any>{
    return this.http.post(`${this.apiUrl}/updatestatus`,array);
  }
  

  
}