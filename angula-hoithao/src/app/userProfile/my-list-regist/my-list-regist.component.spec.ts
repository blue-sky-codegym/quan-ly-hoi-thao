import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyListRegistComponent } from './my-list-regist.component';

describe('MyListRegistComponent', () => {
  let component: MyListRegistComponent;
  let fixture: ComponentFixture<MyListRegistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyListRegistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyListRegistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
