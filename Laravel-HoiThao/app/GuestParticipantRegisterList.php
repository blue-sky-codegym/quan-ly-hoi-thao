<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class GuestParticipantRegisterList extends Model implements AuditableContract
{
    use \OwenIt\Auditing\Auditable;

    protected $table='guestParticipantRegisterList';
    protected $fillable =['name','adress','email','dob','phone','content','closeTheList','seminar_id'];
    public function seminars(){
        return $this->hasMany(Seminar::class);
    }

    protected $auditInclude = [
        'name',
        'email',
    ];
}
