export class InfomationUser {
    id:number;
    name: string;
    email: string;
    adress: string;
    phone: number;
    dob: Date;
    about: string
    image: string;
  accessSeminar: boolean;
  viewLog: boolean;
  editSeminar: boolean;
  editUser: boolean;
  editCategory: boolean;
  editComment: boolean;
  inviteSeminar: boolean;
  editSponsor: boolean;
  accessAdminPage: boolean;
  editContact: boolean;
  addCategory: boolean;
  addAds: boolean;
  listRegister: boolean;
    // password:number;
    // email_verified_at:string;
    // gender:string;
    // accessSeminar:string;
    // viewLog:string;
    // editSeminar:string;
    // editBanner:string;
    // editContactInfo:string;
    // EditAds:string;
    // inviteSeminar:string;
    // editParticipateList:string;
    // editUser:string;
    // editCategory:string;
    // editSponsor:string;
    // editComment:string;
    // rememberToken:string
}