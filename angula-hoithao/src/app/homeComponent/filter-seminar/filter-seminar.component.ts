import { Component, OnInit, Input } from '@angular/core';
import { Seminar } from 'app/Model/Seminar.module';
import { SeminarService } from 'app/Sevice/Seminar/seminar.service';
import { CategoryService } from 'app/Sevice/category/category.service';
import { Category } from 'app/Model/category';
import { Subscription } from 'rxjs';
import { Params, Route, ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-filter-seminar',
    templateUrl: './filter-seminar.component.html',
    styleUrls: ['./filter-seminar.component.scss']
})
export class FilterSeminarComponent implements OnInit {
    seminars: Seminar[];
    categorys: Category[];
    allSeminars: Seminar[];
    public DateFrom: Date;
    public DateTo: Date;
    public keyword: string;
    public subscriptionParams: Subscription

    constructor(
        private seminarService: SeminarService,
        private categoryService: CategoryService,
        private route: ActivatedRoute,
        private sanitizer: DomSanitizer
    ) { }

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            this.keyword = params["searchText"];
            this.seminarService.getSeminar().subscribe(data => {
                this.allSeminars = data;
                this.seminars = data.filter(item => item.seminarName.toLowerCase().indexOf(this.keyword) > -1);
                this.substractContent();
                window.scrollTo(0, 0);
            })
        })
    }

    onChangeStartDate() {
        if (!this.DateTo && this.DateFrom) {
            this.seminars = this.allSeminars.filter(item => this.DateFrom <= item.heldTime);
            this.substractContent();
        }
        else if (!this.DateFrom && this.DateTo) {
            this.seminars = this.allSeminars.filter(item => item.heldTime <= this.DateTo);
            this.substractContent();
        }
        else if (this.DateFrom && this.DateTo) {
            this.seminars = this.allSeminars.filter(item => ((this.DateFrom <= item.heldTime) && (item.heldTime <= this.DateTo)));
            this.substractContent();
        }
    }

    pageChange() {
        window.scrollTo(0, 0);
    }

    substractContent() {
        for (var i in this.seminars) {
            this.seminars[i].content = this.seminars[i].content.substr(0, 500) + '...';
            this.seminars[i].content = this.sanitizer.bypassSecurityTrustHtml(this.seminars[i].content);
        }
    }
}

