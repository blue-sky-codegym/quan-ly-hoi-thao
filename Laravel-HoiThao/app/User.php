<?php

namespace App;
use App\Ads;
use App\AuditLog;
use App\Banner;
use App\Categorys;
use App\Comment;
use App\Contact;
use App\GuestParticipantRegisterList;
use App\ImageSeminar;
use App\InterestList;
use App\Invite;
use App\ParticipantRegisterList;
use App\RegisterRole;
use App\Seminar;
use App\SponsorRegisterList;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
// use OwenIt\Auditing\Contracts\UserResolver;
use Auth;


use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject, AuditableContract
{
    use Notifiable, \OwenIt\Auditing\Auditable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','adress','phone','dob','about','image',
        'gender','accessSeminar','viewLog','editSeminar','editBanner','editContactInfo',
         'EditAds','inviteSeminar','editParticipateList','editUser','editCategory','editSponsor','editComment'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

      // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function registerRole(){
        return $this->belongsTo(RegisterRole::class);
    }

    public function comment()
    {
        return $this->belongsToMany(Seminar::class, 'user_seminars_comment')->withPivot('comment','userName');
    }

    public function invite()
    {
        return $this->belongsToMany(Seminar::class, 'user_serminars_invite')->withPivot('content');
    }

    public function interstList()
    {
        return $this->belongsToMany(Seminar::class, 'user_seminar_interestList')->withPivot('note');
    }

    public function sponsorRegisterList()
    {
        return $this->belongsToMany(Seminar::class, 'user_seminars_sponsorRegisterList')->withPivot('content','closeTheList');
    }

    public function participantRegisterList()
    {
        return $this->belongsToMany(Seminar::class, 'user_seminars_participantRegisterList')->withPivot('closeTheList');
    }

    // public static function resolveId()
    // {
    //     return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    // }

    protected $auditInclude = [
        'id',
        'name',
        'email',
    ];
}
