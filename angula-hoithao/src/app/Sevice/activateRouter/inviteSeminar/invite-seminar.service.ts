import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenService } from 'app/authComponent/authSevice/token.service';

@Injectable({
  providedIn: 'root'
})
export class InviteSeminarService implements CanActivate  {

  user: any;
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    this.user = JSON.parse(localStorage.getItem('user'));
    if(this.user.inviteSeminar == true ){     
      return true;
    } else {
      return false;
    }
  }
  constructor(private Token: TokenService) { }
}
