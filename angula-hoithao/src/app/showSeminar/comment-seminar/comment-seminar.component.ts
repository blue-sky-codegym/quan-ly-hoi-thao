import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'app/Sevice/userSevice/user.service';
import { SeminarService } from './../../Sevice/Seminar/seminar.service';
import { User } from 'app/user/user';
import { InfomationUserrService } from 'app/Sevice/infomationUse/infomation-userr.service';
import { InfomationUser } from 'app/Model/infomationUser.module';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ChangeStatusService } from './../../authComponent/authSevice/change-status.service';

import { AlertService } from 'ngx-alerts';
declare var $: any;

@Component({
  selector: 'app-comment-seminar',
  templateUrl: './comment-seminar.component.html',
  styleUrls: ['./comment-seminar.component.scss'],

})

export class CommentSeminarComponent implements OnInit {
  all: any;
  public loggedIn: boolean;
  public show: boolean = false;
  public editBar: any = 'Show';
  editForm: FormGroup;
  private showedit: boolean;
  private ishowedit: boolean;
  public form = {
    comment: null,
    userName: null,
    user_id: null,
    seminar_id: null
  };

  public user: InfomationUser[] = [];
  public error = [];
  public comment: any;
  private showeditid: any;
  private indexedit: any;
  private ishoweditdiv: boolean;

  constructor(
    private ChangeStatus: ChangeStatusService,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private seminarSevice: SeminarService,
    private infomationUserrService: InfomationUserrService,
    private fb: FormBuilder,
    private router: Router,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    let user = JSON.parse(localStorage.getItem('user'));
    if (user) {
      this.showeditid = user.id;
    }

    console.log(this.showeditid)
    this.edittingForm();
    this.getAllComment();
    this.ChangeStatus.authStatus.subscribe(value => this.loggedIn = value);
    this.showedit = true;
    this.ishowedit = true;
    this.ishoweditdiv = true;
  }

  onSubmit() {
    if (this.loggedIn) {
      this.userService.commentUser(this.form).subscribe(
        data => {
          this.comment.unshift(data)
          this.form.comment = ''
          this.getAllComment();
        },
        error => this.handleError(error)
      );
    } else {
      this.alertService.warning('Bạn vui lòng đăng nhập trước khi bình luận')
    }

  }

  deleteComment(id) {
    this.seminarSevice.deleteComment(id).subscribe(
      data => this.comment = this.comment.filter(t => t.id !== id),
      error => this.alertService.warning('Bình luận không tồn tại ')
    )
  }

  onUpdateComment(id, i) {
    this.seminarSevice.updateComment(this.editForm.value, id).subscribe(data => {
      this.getAllComment();
    })
    this.indexedit = !i;
    this.ishowedit = true;
    this.ishoweditdiv = true;

  }

  handleError(error) {
    this.error = error.error.errors;
  }

  toggle(item, i) {
    this.indexedit = i;
    this.ishowedit = false;
    this.ishoweditdiv = true;
  }

  // CHANGE THE NAME OF THE BUTTON.
  cancelEdit(item, i) {
    this.indexedit = !i;
    this.ishowedit = true;
    this.ishoweditdiv = true;
    this.getAllComment();
  }

  edittingForm() {
    this.editForm = this.fb.group({
      comment: ['', [Validators.required]]
    })
  }

  getAllComment() {
    const id = +this.activatedRoute.snapshot.paramMap.get('id');
    let user = JSON.parse(localStorage.getItem('user'));
    if (user) {
      this.form.userName = user.name;
      this.form.user_id = user.id;
      this.form.seminar_id = id;
    }

    this.seminarSevice.getComment(id).subscribe(
      data => {
        this.comment = data
      },
    )
  }
}
