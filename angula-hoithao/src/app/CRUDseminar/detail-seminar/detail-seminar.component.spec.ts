import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailSeminarComponent } from './detail-seminar.component';

describe('DetailSeminarComponent', () => {
  let component: DetailSeminarComponent;
  let fixture: ComponentFixture<DetailSeminarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailSeminarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailSeminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
