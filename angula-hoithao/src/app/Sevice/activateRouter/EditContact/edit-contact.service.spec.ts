import { TestBed } from '@angular/core/testing';

import { EditContactService } from './edit-contact.service';

describe('EditContactService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditContactService = TestBed.get(EditContactService);
    expect(service).toBeTruthy();
  });
});
