import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HoithaosapdienraComponent } from './hoithaosapdienra.component';

describe('HoithaosapdienraComponent', () => {
  let component: HoithaosapdienraComponent;
  let fixture: ComponentFixture<HoithaosapdienraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HoithaosapdienraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HoithaosapdienraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
