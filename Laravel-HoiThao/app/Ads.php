<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    protected $table='Ads';
    protected $fillable= ['name', 'content','image','URL','status'];
}
