import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoneSeminarComponent } from './done-seminar.component';

describe('DoneSeminarComponent', () => {
  let component: DoneSeminarComponent;
  let fixture: ComponentFixture<DoneSeminarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoneSeminarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoneSeminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
