import { Component, OnInit } from '@angular/core';
import { Seminar } from 'app/Model/Seminar.module';
import { SeminarService } from 'app/Sevice/Seminar/seminar.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CategoryService } from 'app/Sevice/category/category.service';
import { Category } from 'app/Model/category';
import { AlertService } from 'ngx-alerts';

declare var $: any;
declare var jQuery: any;
declare var tinymce: any;

@Component({
  selector: 'app-edit-seminar',
  templateUrl: './edit-seminar.component.html',
  styleUrls: ['./edit-seminar.component.scss']
})
export class EditSeminarComponent implements OnInit {
  public category: Category[];
  public seminar: Seminar;
  public editForm: FormGroup;
  public image: string;

  constructor(private seminarService: SeminarService,
    private categoryService: CategoryService,
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService) { }

  ngOnInit() {
    this.getEdit();
    this.editForm = this.fb.group({
      seminarName: ['', [Validators.required, Validators.minLength(4)]],
      content: [''],
      host: ['', [Validators.required, Validators.minLength(3)]],
      adress: ['', [Validators.required, Validators.minLength(3)]],
      heldTime: ['', [Validators.required]],
      startTime: ['', [Validators.required]],
      image: [''],
      registrationTime: ['', [Validators.required]],
      capacity: ['', [Validators.required, Validators.min(10)]],
      categorys_id: ['', [Validators.required]],
    }, { validator: this.dateLessThan('registrationTime', 'heldTime') });

    this.categoryService.getListCategory().subscribe(data => {
      this.category = data;
    });
  }

  getEdit() {
    const id = +this.activatedRoute.snapshot.paramMap.get('id');
    this.seminarService.getSeminarByid(id).subscribe(
      next => {
        this.image = next.image;
        next.image = null
        this.seminar = next;
        $("#full-featured").val(next.content);
        this.editForm.patchValue(this.seminar);
        jQuery.getScript("../../assets/tinyMCE.js");
        tinymce.remove();
      },
      error => {
        console.log(error);
        this.seminar = null;
      }
    );
  }

  onSubmit() {
    if (this.editForm.value.image == null) {
      this.editForm.value.image = this.image;
    }
    this.editForm.value.content = $('#subValue').val();
    this.seminarService.updateSeminar(this.editForm.value, this.seminar.id).subscribe(
      next => (
        this.alertService.success('Bạn đã sửa thành công'),
        this.router.navigate(['/admin/table'])
      ),
      error => console.log(this.editForm.value)
    )
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader: any = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        this.editForm.value.image = event.target.result;
        this.image = event.target.result;
      }
    }
  }

  dateLessThan(from: string, to: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let f = group.controls[from];
      let t = group.controls[to];
      if (f.value > t.value) {
        return {
          dates: "Ngày đăng ký phải sớm hơn ngày tổ chức hội thảo"
        };
      }
      return {};
    }
  }
}

