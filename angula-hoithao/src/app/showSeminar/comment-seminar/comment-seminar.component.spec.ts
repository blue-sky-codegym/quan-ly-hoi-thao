import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentSeminarComponent } from './comment-seminar.component';

describe('CommentSeminarComponent', () => {
  let component: CommentSeminarComponent;
  let fixture: ComponentFixture<CommentSeminarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentSeminarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentSeminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
