import { Component, OnInit } from '@angular/core';
import { Seminar } from 'app/Model/Seminar.module';
import { SeminarService } from 'app/Sevice/Seminar/seminar.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from 'app/Sevice/category/category.service';
import { Category } from 'app/Model/category';
import { ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Feckback } from 'app/Model/Feckback.model';
import { ContactService } from 'app/Sevice/Contact/contact.service';
import { AlertService } from 'ngx-alerts';

@Component({
  selector: 'app-next-seminar',
  templateUrl: './next-seminar.component.html',
  styleUrls: ['./next-seminar.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class NextSeminarComponent implements OnInit {
  seminars: Seminar[] = [];
  categorys: Category[] = [];
  seminarsHightlight: Seminar[] = [];
  FeckForm: FormGroup;
  feckBack: Feckback;
  countSeminar: number;
  countDoneSeminar: number = 0;
  countNextSeminar: number = 0;
  doneSeminar: Seminar[];

  constructor(
    private seminarService: SeminarService,
    private categoryService: CategoryService,
    private sanitizer: DomSanitizer,
    private contactService: ContactService,
    private fb: FormBuilder,
    private alertService: AlertService,
  ) { }

  ngOnInit() {

    this.FeckForm = this.fb.group({
      yourName: ['', [Validators.required, Validators.minLength(4)]],
      email: ['', [Validators.required, Validators.email]],
      subject: ['', [Validators.required, Validators.minLength(4)]],
      content: ['', [Validators.required, Validators.minLength(4)]],
    })

    this.seminarService.getSeminar().subscribe(
      data => {
        this.seminars = data;
        for (var i in this.seminars) {
          this.seminars[i].content = this.seminars[i].content.substr(0, 500) + '...';
          this.seminars[i].content = this.sanitizer.bypassSecurityTrustHtml(this.seminars[i].content);
        };

        for (var j in data) {
          if (data[j].Highlights) {
            // data[j].content = data[j].content.substr(0, 500) + '...';
            // data[j].content = this.sanitizer.bypassSecurityTrustHtml( data[j].content);
            this.seminarsHightlight.push(data[j]);
          }
        }

        for (var k = 1; k <= this.seminars.length; k++) {
          this.countSeminar = k;
        }
      },
      error => alert("Mất kết nối service"));
    this.categoryService.getListCategory().subscribe(data => {
      this.categorys = data
    });

    this.seminarService.getSeminardone().subscribe(
      data => {
        for (var d = 1; d <= data.length; d++) {
          this.countDoneSeminar = d;
        }
      }
    )

    this.seminarService.getSeminar().subscribe(
      data => {
        this.countNextSeminar = this.countSeminar - this.countDoneSeminar;
      }
    )

  }

  onSubmit() {
    this.contactService.addFeckback(this.FeckForm.value).subscribe(data => (
      this.alertService.success('Cám ơn bạn đã góp ý cho chúng tôi'),
      this.FeckForm.reset()
    ))
  }

}
