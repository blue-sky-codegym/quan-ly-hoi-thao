import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { ads } from 'app/Model/ad.module';


@Injectable({
  providedIn: 'root'
})
export class SendMailService {
  apiUrl = "http://127.0.0.1:8000/api/sendmail";

  constructor(private http: HttpClient) { }
  
  
  sendmail(sendmail):Observable<any> {
    console.log(sendmail)
    return this.http.post(`${this.apiUrl}`, sendmail)
  }

  
}
