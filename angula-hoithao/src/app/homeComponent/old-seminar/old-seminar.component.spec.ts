import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OldSeminarComponent } from './old-seminar.component';

describe('OldSeminarComponent', () => {
  let component: OldSeminarComponent;
  let fixture: ComponentFixture<OldSeminarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OldSeminarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OldSeminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
