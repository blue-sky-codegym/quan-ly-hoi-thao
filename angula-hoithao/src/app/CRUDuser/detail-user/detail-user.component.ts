import { Component, OnInit } from '@angular/core';
import { InfomationUser } from 'app/Model/infomationUser.module';
import { InfomationUserrService } from 'app/Sevice/infomationUse/infomation-userr.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail-user',
  templateUrl: './detail-user.component.html',
  styleUrls: ['./detail-user.component.scss']
})
export class DetailUserComponent implements OnInit {
  public idUpdate: any;
  public users: InfomationUser;
  public user: any;
  public userlocalstorage: any;
  public rolesUser = {
    accessSeminar: false,
    viewLog: false,
    editSeminar: false,
    editUser: false,
    addCategory: false,
    editComment: false,
    inviteSeminar: false,
    editSponsor: false,
    accessAdminPage: false,
    editContact: false,
    addAds: false,
    listRegister: false
  };

  constructor(
    private infomationService: InfomationUserrService,
    private router: ActivatedRoute,
  ) { }

  ngOnInit() {

    const id = +this.router.snapshot.paramMap.get('id');
    this.infomationService.getbyIdUser(id).subscribe(
      data => {
        this.users = data
        this.idUpdate = this.users.id
        this.rolesUser = {
          accessSeminar: this.users.accessSeminar,
          viewLog: this.users.viewLog,
          editSeminar: this.users.editSeminar,
          editUser: this.users.editUser,
          addCategory:this.users.addCategory,
          editComment: this.users.editComment,
          inviteSeminar: this.users.inviteSeminar,
          editSponsor: this.users.editSponsor,
          accessAdminPage: this.users.accessAdminPage,
          editContact: this.users.editContact,
          addAds: this.users.addAds,
          listRegister: this.users.listRegister
        };
      },
      error => { console.log(error) }
    );
  }
  onSubmit() {
    this.userlocalstorage = JSON.parse(localStorage.getItem('user'));

    if (this.userlocalstorage.name == "admin") {
      this.infomationService.addRoles(this.rolesUser, this.idUpdate).subscribe();
      console.log('ok1231')
    } else {
      alert('Bạn ko đủ quyền thực hiện hành động này')
    }


  }
}
